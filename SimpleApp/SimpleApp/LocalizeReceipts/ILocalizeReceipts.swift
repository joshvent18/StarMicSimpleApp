//
//  ILocalizeReceipts.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 10/13/22.
//

import UIKit

class ILocalizeReceipts {
    
    func appendTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) { // -> We do not use this right now
//        appendTextReceiptData(builder, utf8: utf8)
    }

    // ALL ABSTRACT METHODS
    
    func append3inchConnectionTextReceiptData(_ builder: ISCBBuilder, utf8: Bool, model: String, port: String, mac: String) {
    }
    
    func append3inchConnectionRasterReceiptData(_ builder: ISCBBuilder, utf8: Bool, model: String, port: String, mac: String) {
    }
    
    func append3inchTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
    }
    
    func append3inchHospitalityTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
    }
    
    func append2inchTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
    }
    
    func append2InchHospitalityReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
    }
    
    func append39mmTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
    }
    
    func append25mmTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
    }
    
    func triggerPeripheral(_ builder: ISCBBuilder, utf8: Bool) {
    }
        
    func createRasterReceiptData() -> UIImage? {
        return nil
    }
    
    func createRasterHospitalityReceiptData() -> UIImage? {
        return nil
    }
    
    func create39mmRasterReceiptData(_ builder: ISCBBuilder) {
    }
    
    func createPrintableAreaData(_ builder: ISCBBuilder, type: SCBPrintableAreaType) {
    }
    
    // MARK: - imageWithString(); renders a string into an image
    static func imageWithString(_ string: String, font: UIFont, width: CGFloat) -> UIImage  {
        let attributeDictionary: NSDictionary = NSDictionary(dictionary: [NSAttributedString.Key.font : font])
        
        let stringDrawingOptions: NSStringDrawingOptions = [NSStringDrawingOptions.usesLineFragmentOrigin, NSStringDrawingOptions.truncatesLastVisibleLine]
        
        let size: CGSize = (string.boundingRect(with: CGSize(width: width, height: 10000), options: stringDrawingOptions, attributes: attributeDictionary as? [NSAttributedString.Key : Any], context: nil)).size
        
        if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
            if UIScreen.main.scale == 2.0 {
                UIGraphicsBeginImageContextWithOptions(size, false, 1.0)
            } else {
                UIGraphicsBeginImageContext(size)
            }
        } else {
            UIGraphicsBeginImageContext(size)
        }
        
        let context: CGContext = UIGraphicsGetCurrentContext()!
        
        UIColor.white.set()
        
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width + 1, height: size.height + 1)
        
        context.fill(rect)
        
        let attributes: NSDictionary = NSDictionary(dictionary: [NSAttributedString.Key.foregroundColor : UIColor.black, NSAttributedString.Key.font : font])
        
        string.draw(in: rect, withAttributes: attributes as? [NSAttributedString.Key : Any])
        
        let imageToPrint: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return imageToPrint
    }
    
}
