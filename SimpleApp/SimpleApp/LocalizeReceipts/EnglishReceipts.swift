//
//  EnglishReceipts.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 10/13/22.
//

import UIKit

class EnglishReceipts: ILocalizeReceipts {
    
    // MARK: - 3 INCHES / 80MM
    override func append3inchTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
        let encoding: String.Encoding
        
        if utf8 == true {
            encoding = String.Encoding.utf8
            
            builder.append(SCBCodePageType.UTF8)
        }
        else {
            encoding = String.Encoding.ascii
            
            builder.append(SCBCodePageType.CP998)
        }
        
        builder.append(SCBInternationalType.USA)
        
        builder.appendCharacterSpace(0)
        
        builder.append(SCBPrintableAreaType.standard)
        builder.appendAlignment(SCBAlignmentPosition.center)
        
        builder.append((
            "Star Pro Apparel\n" +
            "123 Rainbow Road\n" +
            "Mushroom Kingdom, NJ 12345\n" +
            "\n").data(using: encoding))
        
        builder.appendAlignment(SCBAlignmentPosition.left)
        
        builder.append((
            "Date: 10/12/2022                  Time: 03:54 PM\n" +
            "------------------------------------------------\n" +
            "\n").data(using: encoding))
        
        builder.appendData(withEmphasis: "SALE \n".data(using: encoding))
        
        builder.append((
            "SKU               Description              Total\n" +
            "300678566         LUIGI T-SHIRT            10.99\n" +
            "300692003         PEACH DENIM              29.99\n" +
            "300651148         TOAD DENIM               29.99\n" +
            "300642980         BOWSER LS TEE            49.99\n" +
            "300638471         BOB-OMB BOOTS            35.99\n" +
            "\n" +
            "Subtotal                                  156.95\n" +
            "Tax                                         0.00\n" +
            "------------------------------------------------\n").data(using: encoding))
        
        builder.append("Total                       ".data(using: encoding))
        
        builder.appendData(withMultiple: "   $156.95\n".data(using: encoding), width: 2, height: 2)
        
        builder.append((
            "------------------------------------------------\n" +
            "\n" +
            "Charge\n" +
            "156.95\n" +
            "Visa XXXX-XXXX-XXXX-0123\n" +
            "\n").data(using: encoding))
        
        builder.appendData(withInvert: "Refunds and Exchanges\n".data(using: encoding))
        
        builder.append("Within ".data(using: encoding))
        
        builder.appendData(withUnderLine: "30 days".data(using: encoding))
        
        builder.append(" with receipt\n".data(using: encoding))
        
        builder.append((
            "And tags attached\n" +
            "\n").data(using: encoding))
        
        builder.appendAlignment(SCBAlignmentPosition.center)
                
        builder.appendBarcodeData("{BStar.".data(using: String.Encoding.ascii),
                                  symbology: SCBBarcodeSymbology.code128,
                                  width: SCBBarcodeWidth.mode2,
                                  height: 40,
                                  hri: true)
        
        // kick open cash drawer
        builder.appendPeripheral(SCBPeripheralChannel.no1)
        // builder.appendPeripheral(SCBPeripheralChannel.no2)
                        
    }
        
    override func append3inchHospitalityTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
        let encoding: String.Encoding
        
        if utf8 == true {
            encoding = String.Encoding.utf8
            
            builder.append(SCBCodePageType.UTF8)
        }
        else {
            encoding = String.Encoding.ascii
            
            builder.append(SCBCodePageType.CP998)
        }
        
        builder.append(SCBInternationalType.USA)
        
        builder.appendCharacterSpace(0)
        
        builder.append(SCBPrintableAreaType.standard)
        builder.appendAlignment(SCBAlignmentPosition.center)
        
        builder.append((
            "Five Star Bar\n" +
            "456 Rainbow Road\n" +
            "Mushroom Kingdom, NJ 12345\n" +
            "\n").data(using: encoding))
        
        builder.appendAlignment(SCBAlignmentPosition.left)
        
        builder.append((
            "Server: JOSHUA VENTOCILLA                       \n" +
            "Check: #34                                      \n" +
            "Ordered: 9/12/2023                Time: 03:55 PM\n" +
            "------------------------------------------------\n" +
            "\n").data(using: encoding))
        
        builder.appendData(withEmphasis: "SALE \n".data(using: encoding))
        
        builder.append((
            "Description                                Total\n\n" +
            "LUIGI'S LONG ISLAND                        17.00\n" +
            "PEACH'S PINA COLADA                        20.00\n" +
            "TOAD'S TEQUILA SUNRISE                     13.00\n" +
            "BOWSER'S BLACK MARTINI                     30.00\n" +
            "MARIO'S MIMOSA                             15.00\n" +
            "\n" +
            "Subtotal                                   95.00\n" +
            "Tax                                         0.00\n" +
            "------------------------------------------------\n").data(using: encoding))
        
        builder.append("Total                       ".data(using: encoding))
        
        builder.appendData(withMultiple: "   $95.00\n".data(using: encoding), width: 2, height: 2)
        
        builder.append((
            "------------------------------------------------\n" +
            "\n" +
            "Charge\n" +
            "95.00\n" +
            "Visa XXXX-XXXX-XXXX-0123\n" +
            "\n").data(using: encoding))
        
        
        builder.appendAlignment(SCBAlignmentPosition.center)
        builder.appendData(withInvert: "THANKS FOR GRABBING DRINKS WITH US!\n".data(using: encoding))
        builder.appendBarcodeData("{BStar.".data(using: String.Encoding.ascii),
                                  symbology: SCBBarcodeSymbology.code128,
                                  width: SCBBarcodeWidth.mode2,
                                  height: 40,
                                  hri: true)
        
        // kick open cash drawer
        builder.appendPeripheral(SCBPeripheralChannel.no1)
        // builder.appendPeripheral(SCBPeripheralChannel.no2)

    }
        
    //MARK: - 2 INCHES / 58MM
    override func append2inchTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
        let encoding: String.Encoding
        
        if utf8 == true {
            encoding = String.Encoding.utf8
                   
            builder.append(SCBCodePageType.UTF8)
        } else {
            encoding = String.Encoding.ascii
                   
            builder.append(SCBCodePageType.CP998)
        }
               
        builder.append(SCBInternationalType.USA)
               
        builder.appendCharacterSpace(0)
               
        builder.append(SCBPrintableAreaType.type3)
        builder.appendAlignment(SCBAlignmentPosition.center)
               
        builder.append((
            "Star Clothing Boutique\n" +
            "123 Star Road\n" +
            "City, State 12345\n" +
            "\n").data(using: encoding))
               
        builder.appendAlignment(SCBAlignmentPosition.left)
               
        builder.append((
            "Date: 10/12/2022  Time: 03:54 PM\n" +
            "--------------------------------\n" +
            "\n").data(using: encoding))
               
            builder.appendData(withEmphasis: "SALE\n".data(using: encoding))
            
            builder.append((
                "SKU         Description    Total\n" +
                "300678566   LUIGI T-SHIRT  10.99\n" +
                "300692003   PEACH DENIM    29.99\n" +
                "300651148   TOAD DENIM     29.99\n" +
                "300642980   BOWSER LS TEE  49.99\n" +
                "300638471   BOB-OMB BOOTS  35.99\n" +
                "\n" +
                "Subtotal                  156.95\n" +
                "Tax                         0.00\n" +
                "--------------------------------\n").data(using: encoding))
               
            builder.append("Total     ".data(using: encoding))
               
            builder.appendData(withMultiple: "   $156.95\n".data(using: encoding), width: 2, height: 2)
            
            builder.appendAlignment(SCBAlignmentPosition.left)
            builder.append((
                "--------------------------------\n" +
                "\n" +
                "Charge\n" +
                "156.95\n" +
                "Visa XXXX-XXXX-XXXX-0123\n" +
                "\n").data(using: encoding))
        
            builder.appendData(withInvert: "Refunds and Exchanges\n".data(using: encoding))
           
            builder.append("Within ".data(using: encoding))
           
            builder.appendData(withUnderLine: "30 days".data(using: encoding))
           
            builder.append(" with receipt\n".data(using: encoding))
           
            builder.append((
                "And tags attached\n" +
                "\n").data(using: encoding))
           
            builder.appendAlignment(SCBAlignmentPosition.center)
           
            builder.appendBarcodeData("{BStar.".data(using: String.Encoding.ascii), symbology: SCBBarcodeSymbology.code128, width: SCBBarcodeWidth.mode2, height: 40, hri: true)
    
            // kick open cash drawer
            builder.appendPeripheral(SCBPeripheralChannel.no1)
            // builder.appendPeripheral(SCBPeripheralChannel.no2)

    }
    
    override func append2InchHospitalityReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
        let encoding: String.Encoding
        
        if utf8 == true {
            encoding = String.Encoding.utf8
                   
            builder.append(SCBCodePageType.UTF8)
        } else {
            encoding = String.Encoding.ascii
                   
            builder.append(SCBCodePageType.CP998)
        }
               
        builder.append(SCBInternationalType.USA)
               
        builder.appendCharacterSpace(0)
               
        builder.append(SCBPrintableAreaType.type3)
        builder.appendAlignment(SCBAlignmentPosition.center)
               
        builder.append((
            "Five Star Bar\n" +
            "456 Rainbow Road\n" +
            "Mushroom Kingdom, NJ 12345\n" +
            "\n").data(using: encoding))
               
        builder.appendAlignment(SCBAlignmentPosition.left)
               
        builder.append((
            "Server: JOSHUA VENTOCILLA       \n" +
            "Check: #34                      \n" +
            "Ordered: 9/12/2023              \n" +
            "Time: 03:55PM                   \n" +
            "--------------------------------\n" +
            "\n").data(using: encoding))
               
            builder.appendData(withEmphasis: "SALE\n".data(using: encoding))
            
            builder.append((
                "Description                Total\n" +
                "LUIGI'S LONG ISLAND        17.00\n" +
                "PEACH'S PINA COLADA        20.00\n" +
                "TOAD'S TEQUILA SUNRISE     13.00\n" +
                "BOWSER'S BLACK MARTINI     30.00\n" +
                "MARIO'S MIMOSA             15.00\n" +
                "\n" +
                "Subtotal                   95.00\n" +
                "Tax                         0.00\n" +
                "--------------------------------\n").data(using: encoding))
               
            builder.append("Total     ".data(using: encoding))
               
            builder.appendData(withMultiple: "   $95.00\n".data(using: encoding), width: 2, height: 2)
               
            builder.append((
                "--------------------------------\n" +
                "\n" +
                "Charge\n" +
                "95.00\n" +
                "Visa XXXX-XXXX-XXXX-0123\n" +
                "\n").data(using: encoding))
               
        builder.appendAlignment(SCBAlignmentPosition.center)
        builder.appendData(withInvert: "THANKS FOR GRABBING DRINKS WITH US!\n".data(using: encoding))
        builder.appendBarcodeData("{BStar.".data(using: String.Encoding.ascii),
                                  symbology: SCBBarcodeSymbology.code128,
                                  width: SCBBarcodeWidth.mode2, height: 40, hri: true)
    
        // kick open cash drawer
        builder.appendPeripheral(SCBPeripheralChannel.no1)
        // builder.appendPeripheral(SCBPeripheralChannel.no2)

    }
    
    //MARK: - 39MM
    override func append39mmTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
        //                                ESC    GS    #     +     N=4  n1=0  n2=1  n3=0  n4=1   LF    NUL
//        let printWidthSetting: [UInt8] = [0x1B, 0x1D, 0x23, 0x2B, 0x34, 0x30, 0x30, 0x30, 0x35, 0x0A, 0x00]
//        let printWidthExecute: [UInt8] = [0x1B, 0x1D, 0x23, 0x54, 0x30, 0x30, 0x30, 0x30, 0x30, 0x0A, 0x00]
        let printArea: [UInt8]         = [0x1B, 0x1E, 0x41, 0x35]

        let encoding: String.Encoding
        let length: UInt = UInt(printArea.count)

        if utf8 == true {
            encoding = String.Encoding.utf8
                   
            builder.append(SCBCodePageType.UTF8)
        } else {
            encoding = String.Encoding.ascii
                   
            builder.append(SCBCodePageType.CP998)
        }
               
        builder.append(SCBInternationalType.USA)
               
        builder.appendCharacterSpace(0)
                       
        builder.appendRawBytes(printArea, length: length)
        builder.appendEmphasis(true)
        builder.append(("MOBILE 1/1        \n"   +
                        "Items in order: 1 \n\n" +
                        "** BECKY **       \n\n" +
                        "Matcha Latte      \n\n" +
                        "2 Scoops Matcha   \n"   +
                        "w/ Oat Milk       \n"   +
                        "+ Espresso Shot   \n\n" +
                        "2-May-2023 1:06PM").data(using: encoding))
    }
    //MARK: - 25MM
    
    //MARK: - CONNECTION SUCCESS
    override func append3inchConnectionTextReceiptData(_ builder: ISCBBuilder, utf8: Bool, model: String,
                                                       port: String, mac: String) {
        let encoding: String.Encoding
        
        if utf8 == true {
            encoding = String.Encoding.utf8

            builder.append(SCBCodePageType.UTF8)
        }
        else {
            encoding = String.Encoding.ascii
            
            builder.append(SCBCodePageType.CP998)
        }
        
        let bitmapImage = UIImage(named: "star-logo-black")

        builder.append(SCBInternationalType.USA)
        
        builder.appendCharacterSpace(0)
        
        builder.appendAlignment(SCBAlignmentPosition.center)
        
        builder.appendBitmap(withAlignment: bitmapImage, diffusion: true, width: 300, bothScale: true, rotation: .normal, position: .center)
        
        builder.appendAlignment(SCBAlignmentPosition.center)
        builder.append(("\n"         +
                        "\n"         +
                        "Success!"   +
                        "\n"         +
                        "\n"         +
                        "\(model)\n"   +
                        "-- PORT: \(port) --\n"  +
                        "-- MAC: \(mac) --\n").data(using: encoding))
    }
    
    //MARK: - RASTER
    override func append3inchConnectionRasterReceiptData(_ builder: ISCBBuilder, utf8: Bool, model: String, port: String, mac: String) {
       
        let text: String = "\n\n"       +
                           "Success!     "   +
                           "\n\n"       +
                           "\(model)\n" +
                           "-- PORT: \(port) --\n" +
                           "-- MAC: \(mac) --\n"
        
        let font: UIFont = UIFont(name: "Menlo", size: 12 * 2)!
        
        let receiptImage = ILocalizeReceipts.imageWithString(text, font: font, width: 576)

        builder.append(SCBPrintableAreaType.standard)
        builder.appendBitmap(withAlignment: receiptImage, diffusion: true, width: 576, bothScale: true, rotation: .normal, position: .center)
    }
    
    override func createRasterReceiptData() -> UIImage? {
        let textToPrint: String =
        "           Star Pro Apparel           \n" +
        "           123 Rainbow Road           \n" +
        "      Mushroom Kingdom, NJ 12345      \n" +
        "\n" +
        "Date: 10/12/2022        Time: 03:54 PM\n" +
        "--------------------------------------\n" +
        "SALE\n" +
        "SKU            Description       Total\n" +
        "300678566      LUIGI T-SHIRT     10.99\n" +
        "300692003      PEACH DENIM       29.99\n" +
        "300651148      TOAD DENIM        29.99\n" +
        "300642980      BOWSER LS TEE     49.99\n" +
        "30063847       BOMB-OMB BOOTS    35.99\n" +
        "\n" +
        "Subtotal                        156.95\n" +
        "Tax                               0.00\n" +
        "--------------------------------------\n" +
        "Total                          $156.95\n" +
        "--------------------------------------\n" +
        "\n" +
        "Charge\n" +
        "159.95\n" +
        "Visa XXXX-XXXX-XXXX-0123\n" +
        "Refunds and Exchanges\n" +
        "Within 30 days with receipt\n" +
        "And tags attached\n"
        
        let font: UIFont = UIFont(name: "Menlo", size: 12 * 2)!
        
        return ILocalizeReceipts.imageWithString(textToPrint, font: font, width: 576)     // 3inch(576dots)
    }
    
    override func createRasterHospitalityReceiptData() -> UIImage? {
        let textToPrint: String =
        "              Five Star Bar           \n" +
        "             456 Rainbow Road         \n" +
        "        Mushroom Kingdom, NJ 12345    \n" +
        "\n" +
        "Date: 9/12/2023         Time: 03:55 PM\n" +
        "--------------------------------------\n" +
        "SALE\n"                                   +
        "\n"                                       +
        "Description                      Total\n" +
        "LUIGI'S LONG ISLAND              17.00\n" +
        "PEACH'S PINA COLADA              20.00\n" +
        "TOAD'S TEQUILA SUNRISE           13.00\n" +
        "BOWSER'S BLACK MARTINI           30.00\n" +
        "MARIO'S MIMOSA                   15.00\n" +
        "\n" +
        "Subtotal                         95.00\n" +
        "Tax                               0.00\n" +
        "--------------------------------------\n" +
        "Total                           $95.00\n" +
        "--------------------------------------\n" +
        "\n" +
        "Charge\n" +
        "95.00\n" +
        "Visa XXXX-XXXX-XXXX-0123\n" +
        "\n" +
        "  THANKS FOR GRABBING DRINKS WITH US! \n"
        
        let font: UIFont = UIFont(name: "Menlo", size: 12 * 2)!
        
        return ILocalizeReceipts.imageWithString(textToPrint, font: font, width: 576)     // 3inch(576dots)
    }
        
    override func create39mmRasterReceiptData(_ builder: ISCBBuilder) {
        let bitmap = UIImage(named: "39mm-coffee")!
        
        let printArea: [UInt8] = [0x1B, 0x1E, 0x41, 0x35]
        let lengthArea         = UInt(printArea.count)

        builder.appendRawBytes(printArea, length: lengthArea)
        builder.appendBitmap(bitmap, diffusion: true)
    }
        
}

// ------ FOR TESTING -------
//    override func append3inchTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
//        let encoding: String.Encoding
//
//        if utf8 == true {
//            encoding = String.Encoding.utf8
//
//            builder.append(SCBCodePageType.UTF8)
//        }
//        else {
//            encoding = String.Encoding.ascii
//
//            builder.append(SCBCodePageType.CP998)
//        }
//
//        builder.append(SCBInternationalType.USA)
//
//        builder.appendCharacterSpace(0)
//
//        let signedImage = UIImage(named: "signature-example")
//
//        builder.append("LEFT ALIGNED @ original width".data(using: encoding))
//        builder.appendBitmap(withAlignment: signedImage, diffusion: true, position: .left)
//
//        builder.append("CENTER ALIGNED @ 300 width".data(using: encoding))
//        builder.appendBitmap(withAlignment: signedImage, diffusion: true, width: 300, bothScale: true, position: .center)
//
//        builder.append("RIGHT ALIGNED @ 400 width".data(using: encoding))
//        builder.appendBitmap(withAlignment: signedImage, diffusion: true, width: 400, bothScale: true, position: .right)
//
//        builder.appendAlignment(SCBAlignmentPosition.center)
//        builder.append(SCBFontStyleType.A)
//        builder.appendEmphasis(true)
//        builder.append("Some Organization".data(using: encoding))
//        builder.appendLineFeed()
//        builder.appendEmphasis(false)
//        builder.append("Some Address".data(using: encoding))
//        builder.appendLineFeed()
//        builder.appendLineFeed()
//
//        builder.append("Printer Model: SM-T300i".data(using: encoding))
//        builder.appendLineFeed()
//        builder.append("...........".data(using: encoding))
//        builder.appendLineFeed(3)
//
//    }
