//
//  AlertController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/30/22.
//

import Foundation

extension UIViewController {
    
    func alertNoDevicesFound() {
        let confirm = UIAlertController(title: "No Devices Found", message: "Unable to find a device. Make sure that devices are turned on and available to connect.", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            // do nothing
        })
    
        confirm.addAction(ok)
        
        self.present(confirm, animated: true, completion: nil)
    }
    
    func alertIncorrectDevice() {
        let confirm = UIAlertController(title: "Device Setting Mismatch", message: "The device you are trying to connect does not match its settings. Check the device model and try again.", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            // do nothing
        })

        confirm.addAction(ok)

        self.present(confirm, animated: true, completion: nil)
    }
    
    func alertConnectionSuccess(completion: @escaping() -> Void) {
        let confirm = UIAlertController(title: "Connection Successful", message: "The device has been successfully connected.", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            completion()
        })

        confirm.addAction(ok)

        self.present(confirm, animated: true, completion: nil)
    }
    
    func alertDeviceNotConnected() {
        let alert = UIAlertController(title: "Device Not Connected", message: "Connect the device to start printing", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
           
        })

        alert.addAction(ok)

        self.present(alert, animated: true, completion: nil)
    }
    
    
    func alertDisconnectDevice(completion: @escaping() -> Void) {
        let alert = UIAlertController(title: "Disconnect Device?", message: "The device settings will be reset. Printing from this device will be disabled and must be reconnected.", preferredStyle: .alert)

        let cancel     = UIAlertAction(title: "Keep Connection", style: .cancel)
        let disconnect = UIAlertAction(title: "Disconnect", style: .destructive, handler: { (action) -> Void in
            completion()
        })

        alert.addAction(cancel)
        alert.addAction(disconnect)

        self.present(alert, animated: true, completion: nil)
    }
    
    func alertExitTradeShowMode(vc: UINavigationController) {
        let exit = UIAlertController(title: "Exit Tradeshow Mode", message: "Exiting Trade Show Mode will clear the products that have been added and cannot be undone.", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            // do nothing
        })
        
        let confirm = UIAlertAction(title: "Exit Mode", style: .destructive, handler: { (action) -> Void in
            if let savedProducts = UserDefaults.standard.array(forKey: "savedItems") {
                if savedProducts.isEmpty {
                    vc.popViewController(animated: true)
                } else {
                    vc.showActivitySpinner()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        vc.removeSpinner()
                        vc.popViewController(animated: true)
                    }
                }
            }
        })
    
        exit.addAction(ok)
        exit.addAction(confirm)
        
        self.present(exit, animated: true, completion: nil)
    }
    
    func alertIncorrectPassword() {
        let alert = UIAlertController(title: "Incorrect Password", message: "The password you entered is incorrect. Please try again.", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
           
        })

        alert.addAction(ok)

        self.present(alert, animated: true, completion: nil)
    }
    
    func alertMissingField(fieldTitle: String, fieldMessage: String) {
        let alert = UIAlertController(title: fieldTitle, message: fieldMessage, preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
           
        })

        alert.addAction(ok)

        self.present(alert, animated: true, completion: nil)
    }
    
    func alertReceiptSelection(completion: @escaping(Int) -> Void) {
        
        let receiptOptions = ["Print retail receipt", "Print hospitality receipt", "Print 39mm receipt"]
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
        for options in receiptOptions {
            alert.addAction( UIAlertAction(title: options, style: .default, handler: { action in
                switch action.title {
                case "Print retail receipt":
                    completion(1)
                case "Print hospitality receipt":
                    completion(2)
                case "Print 39mm receipt":
                    completion(3)
                default:
                    completion(1)
                }
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            self.dismiss(animated: true)
        }))

        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            presenter.permittedArrowDirections = []
        }
        
        self.present(alert, animated: false, completion: nil)
    }
    
    func alertTestPrintFailed(completion: @escaping() -> String) {
        
        let confirm = UIAlertController(title: "Communication Result", message: "Failed to open port.", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
           // go back
        })

        confirm.addAction(ok)

        self.present(confirm, animated: true, completion: nil)
    }
    
    func alertUnknownInterface() {
        let confirm = UIAlertController(title: "Unknown Interface", message: "", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            // do nothing
        })

        confirm.addAction(ok)

        self.present(confirm, animated: true, completion: nil)
    }
    
    func alertFailedConnection() {
        let confirm = UIAlertController(title: "Failed to connect to device", message: "", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            // do nothing
        })

        confirm.addAction(ok)

        self.present(confirm, animated: true, completion: nil)
    }
    
    func alertInvalidLicense() {
        let confirm = UIAlertController(title: "Unable to verify driver's license",
                                        message: "Please scan a valid driver's license for verification.",
                                        preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            // do nothing
        })

        confirm.addAction(ok)

        self.present(confirm, animated: true, completion: nil)
    }
    
//    func showNoDevicesFoundAlert(vc: UIViewController) {
//        let confirm = UIAlertController(title: "No device found.", message: "Could not find any devices available.", preferredStyle: .alert)
//
//        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
//            // do nothing
//        })
//
//        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
//            vc.navigationController?.popViewController(animated: true)
//            vc.dismiss(animated: true)
//        })
//
//        confirm.addAction(ok)
//        confirm.addAction(cancel)
//
//        self.present(confirm, animated: true, completion: nil)
//    }
    
}
