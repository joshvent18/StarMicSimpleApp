//
//  Enums.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/7/22.
//

enum PrinterType: String {
    case impact    = "impact"
    case kiosk     = "kiosk"
    case portable  = "portable"
    case thermal   = "thermal"
}

enum ProductCategory: String {
    case printers    = "printers"
    case cashdrawers = "cashdrawers"
    case scales      = "scales"
    case stands      = "stands"
    case scanners    = "scanners"
}
