//
//  ProductsModel.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 9/17/22.
//

import UIKit

struct ProductsModel: Codable {
    var description: String
    var image: String
    var item: String
    var model: ProductModel
    var type: String
    var tagline: String
}
