//
//  ProductModel.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 10/25/22.
//

import UIKit

struct ProductModel: Codable {
    var modelName: [String]
    var modelNumber: [String]
    var interface: [String]
}
