//
//  Model.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/4/22.
//

import UIKit

struct Model: Codable {
    var description: String
    var image: String
    var item: String
    var model: ModelInformation
    var type: String
    var tagline: String
}
