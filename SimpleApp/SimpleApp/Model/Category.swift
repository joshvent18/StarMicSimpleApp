//
//  ProductCategory.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/4/22.
//

import UIKit

struct Category: Codable {
    var printers: [Model]
    var cashdrawers: [Model]
    var scales: [Model]
    var stands: [Model]
    var scanners: [Model]
}
