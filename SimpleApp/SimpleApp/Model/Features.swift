//
//  Features.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 10/19/22.
//

import UIKit

struct Features: Codable {
    var name: String
    var features: [String]
}
