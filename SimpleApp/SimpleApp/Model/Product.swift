//
//  Product.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 10/25/22.
//

import UIKit

struct Product: Codable {
    var model: ProductModel
    var name: String
    var description: String
    var tagline: String
    var features: [String]
    var image: String
}
