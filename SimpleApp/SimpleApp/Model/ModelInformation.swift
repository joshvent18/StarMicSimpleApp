//
//  ModelInformation.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/4/22.
//

import UIKit

struct ModelInformation: Codable {
    var modelName: [String]
    var modelNumber: [String]
    var modelDescription: [String]
}
