//
//  SMDefine.swift
//  Swift SDK
//
//  Created by Yuji on 2015/**/**.
//  Copyright © 2015年 Star Micronics. All rights reserved.
//

import Foundation

   func MakePrettyFunction(_ filePath: String = #file, line: Int = #line, funcName: String = #function) -> String {
       let fileName: String = filePath.components(separatedBy: "/").last!
       return "-[\(fileName)(\(line)) \(funcName):]"
   }

let sm_true:  UInt32 = 1     // SM_TRUE
let sm_false: UInt32 = 0     // SM_FALSE

//MARK: - DEFINITION FOR MC-LABEL
let paper_80mm: UInt32 = 0
let paper_58mm: UInt32 = 1
let paper_39mm: UInt32 = 2
let paper_25mm: UInt32 = 3

//MARK: - DEFINE INTERFACE OPTIONS
/*
    Check interface type that is currently connected when checking status
 */
let usb_b: UInt32 = 0
let usb_ios_1: UInt32 = 1
let usb_ios_7: UInt32 = 7
let bt: UInt32 = 2
let ethernet: UInt32 = 3
let usb_c_pd_support: UInt32 = 4
let usb_c_pd_nosupport: UInt32 = 5
let usb_a: UInt32 = 6               // Android only!
