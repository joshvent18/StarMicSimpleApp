//
//  SettingsViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 2/2/22.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet var settingsTableView: UITableView!
    @IBOutlet weak var backButtonItem: UIBarButtonItem!
    
    var cellData = [ "Printers",
                     "Hello World",
                     "I Like Pizza" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsTableView.delegate   = self
        settingsTableView.dataSource = self
    }
    
    @IBAction func handleBackButton(_ sender: Any) {
        
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
        
    }
    
    func showAlert(withTitle title: String, withMessage message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
                
    }
    
    func navigateToPrinterView() {
        
        let printerVB = UIStoryboard.init(name: "CloudPrint", bundle: nil)
        let printerVC = printerVB.instantiateViewController(withIdentifier: "CloudPrintViewController") as! CloudPrintViewController
        
        printerVC.modalTransitionStyle   = .flipHorizontal
        printerVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(printerVC, animated: true)
        
    } //: navigateToPrinterView
    
    func printOptions() {
        
        let printerVB = UIStoryboard.init(name: "PrintOptions", bundle: nil)
        let printerVC = printerVB.instantiateViewController(withIdentifier: "PrintOptionsView") as! PrintOptionsView
        
        printerVC.modalTransitionStyle   = .flipHorizontal
        printerVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(printerVC, animated: true)
        
    } //: printOptions
    
    func navigateToHelloWorld() {
        
    } //: navigateToHelloWorld
    
    func navigateToILikePizza() {
        
    } //: navigateToILikePizza
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                        
        switch indexPath.row {
        case 0:
            printOptions()
            break
        case 1:
            print("going to hello world")
            break
        case 2:
            print("going to i like pizza")
            break
        default:
            break
        }
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath)
        
        cell.textLabel?.text = cellData[indexPath.row]
        
        return cell
    }

}
