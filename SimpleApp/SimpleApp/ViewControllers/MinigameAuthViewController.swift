//
//  MinigameAuthViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 12/13/22.
//

import UIKit

class MinigameAuthViewController: UIViewController {
    
    //MARK: - IBOUTLET
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    //MARK: - VARS / LETS
    private let pass: String              = "integration"
    private var authenticated: Bool       = false
    private var userEnteredPassword: Bool = false
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyBoardWhenScreenTapped()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        reset()
    }
    
    //MARK: - FUNCTIONS
    func reset() {
        passwordTextField.text = ""
        authenticated          = false
        userEnteredPassword    = false
    }
    
    func authenticate(password: String) -> Bool {
        if password == pass {
            authenticated = true
        } else {
            authenticated = false
        }
        
        return authenticated
    }
    
    func validateTextField(input: String) -> Bool {
        var isValid = false
        if input.isEmpty {
            isValid = false
        } else {
            isValid = true
        }
        
        return isValid
    }
    
    //MARK: - IBACTION
    @IBAction func submitButtonClicked(_ sender: Any) {
        let userInput = passwordTextField.text!
        
        if validateTextField(input: userInput) {
            if authenticate(password: userInput) {
                goToMiniGame()
            } else {
                alertIncorrectPassword()
            }
        } else {
            alertMissingField(fieldTitle: "Missing Password", fieldMessage: "Password field cannot be empty. Please try again.")
        }
    }
    
    //MARK: - TRANSITION
    func goToMiniGame() {
        let minigameSB = UIStoryboard.init(name: "Minigame", bundle: nil)
        let minigameVC = minigameSB.instantiateViewController(withIdentifier: "Minigame") as! MinigameViewController
                                            
        minigameVC.modalTransitionStyle   = .flipHorizontal
        minigameVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(minigameVC, animated: true)
    }
    
}
