//
//  TradeShowMainViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/9/22.
//

import UIKit

// global
var currentPorts: [PrinterInfo]  = []

class TradeShowMainViewController: UIViewController {

    static var scaleVC: ViewController?
    
    @IBOutlet weak var beginAddingLabel: UILabel!
    @IBOutlet weak var addProductLabel: UILabel!
    
    @IBOutlet weak var productsTableView: UITableView!
    
    @IBOutlet weak var addBarButton: UIBarButtonItem!
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var minigameBarButton: UIBarButtonItem!
    @IBOutlet weak var removeAllBarButton: UIBarButtonItem!
    
    var productList: [Model]?
    var products: Category? = nil
    var productsInUse: [String]      = []
    var productImagesInUse: [String] = []
    var productTypeInUse: [String]   = []
    var currentCellIndex: IndexPath  = IndexPath()
    let printerModel: PrinterModel        = PrinterModel()
    
    //MARK: - LIFECYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        productsTableView.register(TradeShowMainTableViewCell.nib(), forCellReuseIdentifier: TradeShowMainTableViewCell.identifier)
        
        productsTableView.delegate   = self
        productsTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        resetData()
        
        productsInUse      = getSavedProducts()
        productTypeInUse   = getProductTypeInUse()
        productImagesInUse = getProductImagesInUse()
        
        reloadData()
        
        if productsInUse.count > 0 {
            addProductLabel.isHidden   = true
            beginAddingLabel.isHidden  = true
            productsTableView.isHidden = false
            productsTableView.reloadData()
            loadPorts()
        } else {
            addProductLabel.text        = "No Items"
            addProductLabel.isHidden    = false
            beginAddingLabel.isHidden   = false
            productsTableView.isHidden  = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        saveCurrentState()
    }
    
    //MARK: - DELETE DATA ( resetData(), deleteItem() )
    func resetData() {
        productsInUse.removeAll()
        productImagesInUse.removeAll()
        productTypeInUse.removeAll()
        currentPorts.removeAll()
    }
    
    func deleteItem(itemIndex: Int) {
        productsInUse.remove(at: itemIndex)
        productTypeInUse.remove(at: itemIndex)
        productImagesInUse.remove(at: itemIndex)
        currentPorts.remove(at: itemIndex)
    }
    
    @IBAction func clearConnectedItems(_ sender: Any) {
        
        if productsInUse.count == 0 {
            var dialogueMessage = UIAlertController(title: "Could Not Clear Items", message: "There are no items to clear. ", preferredStyle: .alert)
            
            let clear = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                // do nothing
            })
            
            dialogueMessage.addAction(clear)
            
            self.present(dialogueMessage, animated: true, completion: nil)
        } else {
            let dialogueMessage = UIAlertController(title: "Remove All Items?",
                                                    message: "Are you sure you want to remove all the items currently added?",
                                                    preferredStyle: .alert)
            
            let clear = UIAlertAction(title: "Remove All", style: .destructive, handler: { (action) -> Void in
                self.resetData()
                self.reloadData()
            })
            
            let cancel = UIAlertAction(title: "Keep All", style: .cancel, handler: { (action) -> Void in
                // do nothing
            })
            
            dialogueMessage.addAction(clear)
            dialogueMessage.addAction(cancel)
            
            self.present(dialogueMessage, animated: true, completion: nil)
        }
    }
    
    @IBAction func reorder(_ sender: Any) {
        productsTableView.setEditing(!productsTableView.isEditing, animated: true)
        editButton.title = productsTableView.isEditing ? "Done" : "Edit"
    }
    
        
    //MARK: - TRANSITIONS ( transitionSearchPort(), transitionProductSelection(), enterMiniGame(), exitTradeShow() )    
    func transitionScannerTextView() {
        let scannerSB = UIStoryboard.init(name: "Scanner", bundle: nil)
        let scannerVC = scannerSB.instantiateViewController(withIdentifier: "ScannerSB") as!
            ScannerViewController
        
        scannerVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(scannerVC, animated: true)
    }
    
    func transitionDeviceStatus(index: Int) {
        let deviceStatusSB = UIStoryboard.init(name: "DeviceStatus", bundle: nil)
        let deviceStatusVC = deviceStatusSB.instantiateViewController(withIdentifier: "DeviceStatusSB") as!
            DeviceStatusViewController
        
        deviceStatusVC.printerName = currentPorts[index].name
        deviceStatusVC.printerPort = currentPorts[index].port
        
        deviceStatusVC.modalPresentationStyle = .pageSheet
        deviceStatusVC.modalTransitionStyle   = .coverVertical
        deviceStatusVC.modalTransitionStyle   = .flipHorizontal
        deviceStatusVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(deviceStatusVC, animated: true)
    }
    
    func transitionSearchPort(index: Int) {
        let searchPortSB = UIStoryboard.init(name: "Search", bundle: nil)
        let searchPortVC = searchPortSB.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
                                
        searchPortVC.searchViewDelegate     = self
        searchPortVC.printerIndex           = index
        searchPortVC.currentPrinter         = currentPorts[index].name
        searchPortVC.modalTransitionStyle   = .flipHorizontal
        searchPortVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(searchPortVC, animated: true)
    }
    
    func transitionSearchScale() {
        let scaleSB = UIStoryboard.init(name: "Scale", bundle: nil)
        let scaleVC = scaleSB.instantiateViewController(withIdentifier: "Scale") as!
            ScaleViewController
        
        scaleVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(scaleVC, animated: true)
    }
        
    @IBAction func enterMiniGame(_ sender: Any) {
        let minigameSB = UIStoryboard.init(name: "Minigame", bundle: nil)
        let minigameVC = minigameSB.instantiateViewController(withIdentifier: "MinigameAuth") as! MinigameAuthViewController
                                            
        minigameVC.modalTransitionStyle   = .flipHorizontal
        minigameVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(minigameVC, animated: true)
    }
    
    // Exit and clear all products including saved
    @IBAction func exitTradeShowTapped(_ sender: Any) {
        if let navController = self.navigationController {
            self.resetData()
            self.alertExitTradeShowMode(vc: navController)
        }
    }
    
    @IBAction func transitionProductSelection(_ sender: Any) {
        let productSelectionSB = UIStoryboard.init(name: "TradeShowProductSelection", bundle: nil)
        let productSelectionVC = productSelectionSB.instantiateViewController(withIdentifier: "TSProductSelection") as! TradeShowProductSelectionViewController
                                            
        productSelectionVC.modalTransitionStyle   = .flipHorizontal
        productSelectionVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(productSelectionVC, animated: true)
    }
    
    //MARK: - SAVE DATA
    func saveCurrentState() {
        UserDefaults.standard.set(productsInUse, forKey: "savedItems")
        UserDefaults.standard.set(productTypeInUse, forKey: "savedItemsType")
        UserDefaults.standard.set(productImagesInUse, forKey: "savedItemsImage")
        saveCurrentPorts()
    }
        
    //MARK: - RELOAD DATA ( reloadData() )
    func reloadData() {
        productsTableView.reloadData()
        
        if productsInUse.count > 0 {
            addProductLabel.isHidden   = true
            beginAddingLabel.isHidden  = true
            productsTableView.isHidden = false
            productsTableView.reloadData()
        } else {
            addProductLabel.text       = "No Items"
            addProductLabel.isHidden   = false
            beginAddingLabel.isHidden  = false
            productsTableView.isHidden = true
        }
    }
    
    //MARK: - PORTS ( loadPorts(), saveCurrentPorts(), getSavedPorts() )
    func loadPorts() {
        // initialize product ports
        // port and macAddress should be unknown if not a printer
        for item in productsInUse {
            currentPorts.append(PrinterInfo(name: item, port: "unknown", mac: "unknown", connected: false))
        }
        
        getSavedPorts { hasPorts, saved in
            // run through each port and save it
            var index = 0
            
            if saved.count > 0 {
                for _ in saved {
                    let savedPort = saved[index].name
                    if currentPorts[index].name.contains(savedPort) {
                        currentPorts[index] = saved[index]
                    }
                    index += 1
                }
            }
        }
    }

    func saveCurrentPorts() {
        let savedPorts = currentPorts
        if let data = try? PropertyListEncoder().encode(savedPorts) {
            UserDefaults.standard.set(data, forKey: "currentlySavedPorts")
        }
    }
    
    func getSavedPorts(completion: @escaping(Bool, [PrinterInfo]) -> Void) {
        let defaults = UserDefaults.standard
        if let data = defaults.data(forKey: "currentlySavedPorts") {
            let array = try! PropertyListDecoder().decode([PrinterInfo].self, from: data)
            completion(true, array)
        } else {
            completion(false, [])
        }
    }
        
    //MARK: - GET DATA ( getSavedProducts(), getProductImagesInUse(), getProductTypeInUse(),
    func getSavedProducts() -> [String] {
        
        var savedItems: [String] = []
        
        if let savedProducts = UserDefaults.standard.array(forKey: "savedItems") {
            for saved in savedProducts {
                let item = saved as! String
                savedItems.append(item)
            }
            return savedItems
        }
        
        // an empty list if nothing is saved
        return []
    }
    
    func getProductImagesInUse() -> [String] {
        
        var savedImages: [String] = []
        
        if let savedProductImages = UserDefaults.standard.array(forKey: "savedItemsImage") {
            for image in savedProductImages {
                let imageName = image as! String
                savedImages.append(imageName)
            }
            return savedImages
        }
        
        // an empty list if nothing is saved
        return []
    }
    
    func getProductTypeInUse() -> [String] {
        
        var savedType: [String] = []
        
        if let savedProductType = UserDefaults.standard.array(forKey: "savedItemsType") {
            for type in savedProductType {
                let typeString = type as! String
                savedType.append(typeString)
            }
            return savedType
        }
        
        return []
    }
        
    func fetchAllProductInformation(itemType: String,
                                    itemName: String,
                                    completion: @escaping(ModelInformation, String, String, String, String, String)-> Void)
    {
        
        guard let path = Bundle.main.path(forResource: "category", ofType: "json") else {
            return
        }
                
        let url = URL(fileURLWithPath: path)
        
        do {
            let data = try Data(contentsOf: url)
            products = try JSONDecoder().decode(Category.self, from: data)
        } catch {
            print("Error: \(error)")
        }
        
        guard let id = checkProductType(itemType: itemType) as? Int else {
            return
        }
        
        getDeviceInformation(categoryID: id, itemName: itemName) { found, information, name, modelType, tagline,
            description, image in
            if found {
                completion(information, name, modelType, tagline, description, image)
            } else {
                completion(information, "", "", "", "", "")
            }
        }
        
    }
    
    /*
        Check for product type. Category used is identified by numbers
        0 = printers
        1 = cash drawers
        2 = scales
        3 = stands
        4 = scanners
     */
    func checkProductType(itemType: String) -> Int {
        var type = itemType.lowercased()
        var typeID: Int = 0
        
        if( type == PrinterType.thermal.rawValue || type == PrinterType.impact.rawValue ||
            type == PrinterType.portable.rawValue || type == PrinterType.kiosk.rawValue) {
            typeID = 0
        } else if(itemType == "Cash Drawer") {
            typeID = 1
        } else if(itemType == "POS Scales") {
            typeID = 2
        } else if(itemType == "Stands") {
            typeID = 3
        } else {
            typeID = 4
        }
        return typeID
    }
    
    func checkFor2InchPrinters(model: String) -> Bool {
        let printer = PrinterModel()
        
        switch model {
        case printer.mcp20,
            printer.mcp21:
            return true
        case printer.mpop:
            return true
        case printer.sm_l200,
             printer.sm_s210i,
             printer.sm_s230i:
            return true
        default:
            return false
        }
    }
    
    func getDeviceInformation(categoryID: Int, itemName: String, completion: @escaping(Bool, ModelInformation,
                                                                                       String, String, String,
                                                                                       String, String) -> Void) {
        switch categoryID {
            case 0:
                productList = products?.printers
            case 1:
                productList = products?.cashdrawers
            case 2:
                productList = products?.scales
            case 3:
                productList = products?.stands
            case 4:
                productList = products?.scanners
            default:
                // en empty list
                productList = []
        }
        
        // find the element in the array and get that element
        if let product = productList?.first(where: {$0.item == itemName}) {
            completion(true, product.model, product.item, product.type, product.tagline,
                       product.description, product.image)
        } else {
            let modelInfo = ModelInformation(modelName: [], modelNumber: [], modelDescription: [])
            completion(false, modelInfo, "", "", "", "", "")
        }
    }
    
    /*
        Use to customize navbar
     */
    @available(iOS 13.0, *)
    func customNavBarAppearance() -> UINavigationBarAppearance {
        let customNavBarAppearance = UINavigationBarAppearance()
        
        // Apply a red background.
        customNavBarAppearance.configureWithOpaqueBackground()
        customNavBarAppearance.backgroundColor = .systemRed
        
        // Apply white colored normal and large titles.
        customNavBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        customNavBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]

        // Apply white color to all the nav bar buttons.
        let barButtonItemAppearance = UIBarButtonItemAppearance(style: .plain)
        barButtonItemAppearance.normal.titleTextAttributes = [.foregroundColor: UIColor.white]
        barButtonItemAppearance.disabled.titleTextAttributes = [.foregroundColor: UIColor.lightText]
        barButtonItemAppearance.highlighted.titleTextAttributes = [.foregroundColor: UIColor.label]
        barButtonItemAppearance.focused.titleTextAttributes = [.foregroundColor: UIColor.white]
        customNavBarAppearance.buttonAppearance = barButtonItemAppearance
        customNavBarAppearance.backButtonAppearance = barButtonItemAppearance
        customNavBarAppearance.doneButtonAppearance = barButtonItemAppearance
        
        return customNavBarAppearance
    }
    
}

//MARK: - DELEGATES

extension TradeShowMainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let row = indexPath.row
        let currentItem = productsInUse[row]
        
        let dialogueMessage = UIAlertController(title: "Remove Device?", message: "\(currentItem) will be removed from the list. If the device has been connected settings will be lost.", preferredStyle: .alert)
        
        let clear = UIAlertAction(title: "Remove", style: .destructive, handler: { (action) -> Void in
            self.deleteItem(itemIndex: row)
            self.reloadData()
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        })
        
        dialogueMessage.addAction(clear)
        dialogueMessage.addAction(cancel)
        
        self.present(dialogueMessage, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
                        
        let moveNameCell  = productsInUse[sourceIndexPath.row]
        let moveImageCell = productImagesInUse[sourceIndexPath.row]
        let moveTypeCell  = productTypeInUse[sourceIndexPath.row]
        let movePorts     = currentPorts[sourceIndexPath.row]
        
        // Reorder cell; product name, product image, product type
        productsInUse.remove(at: sourceIndexPath.row)
        productImagesInUse.remove(at: sourceIndexPath.row)
        productTypeInUse.remove(at: sourceIndexPath.row)
        currentPorts.remove(at: sourceIndexPath.row)
        
        productsInUse.insert(moveNameCell, at: destinationIndexPath.row)
        productImagesInUse.insert(moveImageCell, at: destinationIndexPath.row)
        productTypeInUse.insert(moveTypeCell, at: destinationIndexPath.row)
        currentPorts.insert(movePorts, at: destinationIndexPath.row)
        
        reloadData()      // Reload data for viewing information and selecting proper ports
        
    }
    
}

extension TradeShowMainViewController: UITableViewDataSource {
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsInUse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TradeShowMainTableViewCell.identifier, for: indexPath) as! TradeShowMainTableViewCell
        let cellIndex = indexPath.row
        
        cell.selectionStyle = .none
        cell.cellDelegate   = self
        cell.configureCell(name: productsInUse[cellIndex],
                           type: productTypeInUse[cellIndex],
                           image: productImagesInUse[cellIndex],
                           index: cellIndex,
                           macAddress: currentPorts[cellIndex].mac,
                           isConnected: currentPorts[cellIndex].connected)
                
        return cell
    }
    
}

extension TradeShowMainViewController: TradeShowMainTableViewDelegate {
   
    // this function moved from here to the swipe method (12/14/22) - (commit, forRowAt)
    func delete(_ cell: TradeShowMainTableViewCell) {
        if let cellSelected = cell.cellIndex {
            
            let currentItem = productsInUse[cellSelected]
            
            let dialogueMessage = UIAlertController(title: "Remove Device?", message: "\(currentItem) will be removed from the list. If the device has been connected settings will be lost.", preferredStyle: .alert)
            
            let clear = UIAlertAction(title: "Remove", style: .destructive, handler: { (action) -> Void in
                self.deleteItem(itemIndex: cellSelected)
                self.reloadData()
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
                
            })
            
            dialogueMessage.addAction(clear)
            dialogueMessage.addAction(cancel)
            
            self.present(dialogueMessage, animated: true, completion: nil)
        }
    }
    
    func information(_ cell: TradeShowMainTableViewCell) {
        if let row = cell.cellIndex {
            let name = productsInUse[row]
            let type = productTypeInUse[row]
            
            // get information to show for the product page
            fetchAllProductInformation(itemType: type, itemName: name) { modelInfo, modelName, modelType, modelTagline,
                modelDescription, modelImage in
                
                // transition to product page
                let infoSB = UIStoryboard.init(name: "ProductInfo", bundle: nil)
                let infoVC = infoSB.instantiateViewController(withIdentifier: "ProductInfoViewController") as! ProductInfoViewController
                                             
                infoVC.name                = modelName
                infoVC.type                = modelType
                infoVC.tagline             = modelTagline
                infoVC.productDescription  = modelDescription
                infoVC.modelAndPartNumbers = modelInfo
                infoVC.imageName           = modelImage
                            
                infoVC.modalTransitionStyle   = .flipHorizontal
                infoVC.modalPresentationStyle = .overFullScreen
                self.navigationController?.pushViewController(infoVC, animated: true)
            }
        }
    }
    
    func settings(_ cell: TradeShowMainTableViewCell) {
        if let row = cell.cellIndex {
            let type = productTypeInUse[row].lowercased()
            let item = currentPorts[row]
            
            if type == ProductCategory.scanners.rawValue {
                transitionScannerTextView()
            } else if type == "pos scales" {
                transitionSearchScale()
            } else {
                if item.connected {
                    // disconnect device and save
                    alertDisconnectDevice {
                        currentPorts[row] = PrinterInfo(name: item.name, port: "unknown", mac: "unknown", connected: false)
                        self.saveCurrentPorts()
                        self.reloadData()
                    }
                } else {
                    transitionSearchPort(index: row)
                }
            }
        }
    }
    
    func deviceStatus(cell: TradeShowMainTableViewCell) {
        if let row = cell.cellIndex {
            let portName = currentPorts[row].port
            let printer  = currentPorts[row].name
                        
            transitionDeviceStatus(index: row)
        }
    }

    func testPrint(_ cell: TradeShowMainTableViewCell) {
        
        if let cellSelected = cell.cellIndex {
            
            alertReceiptSelection { receipt_val in
                
                DispatchQueue.main.async {
                    self.showPrintingSpiner()
                }

                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {

                    let portName = currentPorts[cellSelected].port
                    let printer  = currentPorts[cellSelected].name
                    let commands: Data

                    while true {
                        var port : SMPort
                        
                        do {
                            // Open port
                            port = try SMPort.getPort(portName: portName, portSettings: "", ioTimeoutMillis: 10000)

                            // Close port
                            defer {
                                SMPort.release(port)
                            } // defer

                            var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
                            
                            // Start to check the completion of printing
                            try port.beginCheckedBlock(starPrinterStatus: &printerStatus, level: 2)

                            if printerStatus.offline == sm_true {
                                break    // The printer is offline.
                            }
                                                                                                                
                            // NOTE:
                            // 1. Checking for 2in printer by name "mC-Print2" || "SM-S230" || "mPOP" || "SM-L200"
                            // 2. Check for receipt type hospitality or retail
                            if self.checkFor2InchPrinters(model: printer) {
                                commands = PrinterFunctions.create2InchTextReceiptData(StarIoExtEmulation.starPRNT, utf8: true, receiptType: receipt_val)
                            } else {
                                if printer == self.printerModel.tsp100iii  {
                                    commands = PrinterFunctions.createRasterReceiptData(StarIoExtEmulation.starGraphic, utf8: true, receiptType: receipt_val)
                                } else if printer == self.printerModel.mclabel {
                                    
                                    // FOR MC-LABEL
                                    // CHECK PAPER WIDTH AND PRINT BASED ON PAPER SIZE
                                    // Returns 0 for 80mm, 1 for 58mm, 2 for 39mm, and 3 for 25mm
                                    
                                    // Can we go from receipt to detecting black mark???
                                    if printerStatus.detectedPaperWidth == paper_58mm {
                                        commands = PrinterFunctions.create2InchTextReceiptData(StarIoExtEmulation.starPRNT, utf8: true, receiptType: receipt_val)
                                    } else if printerStatus.detectedPaperWidth == paper_39mm {
                                        commands = PrinterFunctions.create39mmTextReceiptData(StarIoExtEmulation.starPRNT, utf8: true, receiptType: receipt_val)
                                    } else if printerStatus.detectedPaperWidth == paper_25mm {
                                        commands = PrinterFunctions.createTextReceiptData(StarIoExtEmulation.starPRNT, utf8: true, receiptType: receipt_val)
                                    } else {
                                        commands = PrinterFunctions.createTextReceiptData(StarIoExtEmulation.starPRNT, utf8: true, receiptType: receipt_val)
                                    }
                                    
                                } else {
                                    
                                    if receipt_val == 3 {
                                        commands = PrinterFunctions.createRasterReceiptData(StarIoExtEmulation.starPRNT, utf8: true, receiptType: receipt_val)
                                    } else {
                                        commands = PrinterFunctions.createTextReceiptData(StarIoExtEmulation.starPRNT, utf8: true, receiptType: receipt_val)
                                    }
                                    
                                }
                            }
                                                        
                            var commandsArray: [UInt8] = [UInt8](repeating: 0, count: commands.count)
                            
                            commands.copyBytes(to: &commandsArray, count: commandsArray.count)
                            
                            var total: UInt32 = 0

                            while total < UInt32(commandsArray.count) {
                                var written: UInt32 = 0

                                // Send print data
                                try port.write(writeBuffer: commandsArray, offset: total,
                                            size: UInt32(commandsArray.count) - total,
                                            numberOfBytesWritten: &written)

                                total += written
                            } // while-loop

                            // Stop to check the completion of printing
                            try port.endCheckedBlock(starPrinterStatus: &printerStatus, level: 2)

                            if printerStatus.offline == sm_true {
                                break    // The printer is offline.
                            }

                            // Success
                            break
                        }
                        catch let error as NSError {
                            break    // Some error occurred.
                        }
                    } // end while

                    self.removeSpinner()
                }
            }
        } // end print function
    }
}

extension TradeShowMainViewController: SearchViewDelegate {
    func printerInformation(model: String, port: String, macAddress: String, index: Int) {
    
        // insert port information and save it
        currentPorts[index] = PrinterInfo(name: model, port: port, mac: macAddress, connected: true)
        saveCurrentPorts()
        
        let portName = currentPorts[index].port
        let commands: Data
        let emulation: StarIoExtEmulation
        
        if model == printerModel.tsp100iii {
            emulation = StarIoExtEmulation.starGraphic
        } else {
            emulation = StarIoExtEmulation.starPRNT
        }
        
        commands = PrinterFunctions.createConnectionSuccessfulTestReceiptData(emulation, utf8: true, modelName: model, portName: port, macAddress: macAddress)
        
        var commandsArray: [UInt8] = [UInt8](repeating: 0, count: commands.count)
                        
        commands.copyBytes(to: &commandsArray, count: commandsArray.count)
                        
        while true {
            var port : SMPort

            do {
                // Open port
                port = try SMPort.getPort(portName: portName, portSettings: "", ioTimeoutMillis: 10000)

                // Close port
                defer {
                    SMPort.release(port)
                } // defer

                var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()

                // Start to check the completion of printing
                try port.beginCheckedBlock(starPrinterStatus: &printerStatus, level: 2)

                if printerStatus.offline == sm_true {
                    break    // The printer is offline.
                }

                var total: UInt32 = 0

                while total < UInt32(commandsArray.count) {
                    var written: UInt32 = 0

                    // Send print data
                    try port.write(writeBuffer: commandsArray, offset: total,
                                size: UInt32(commandsArray.count) - total,
                                numberOfBytesWritten: &written)

                    total += written
                } // while-loop

                // Stop to check the completion of printing
                try port.endCheckedBlock(starPrinterStatus: &printerStatus, level: 2)

                if printerStatus.offline == sm_true {
                    break    // The printer is offline.
                }

                // Success
                break
                }
                catch let error as NSError {
                    break    // Some error occurred.
            }
        } // end print
    }
}

//if let cellSelected = cell.cellIndex {
//
//    DispatchQueue.main.async {
//        self.showPrintingSpiner()
//    }
//
//    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
//
//        let portName = currentPorts[cellSelected].port
//        let printer  = currentPorts[cellSelected].name
//        let commands: Data
//
//        // NOTE:
//        // Checking for 2 inch printer here by name "mC-Print2" or "SM-S230", or "mPOP", or
//        // "SM-L200"
//        if self.checkFor2InchPrinters(model: printer) {
//            commands = PrinterFunctions.create2InchTextReceiptData(StarIoExtEmulation.starPRNT, utf8: true)
//        } else {
//            if printer == PrinterModel.tsp100iii.rawValue {
//                commands = PrinterFunctions.createRasterReceiptData(StarIoExtEmulation.starGraphic, utf8: true)
//            } else {
//                commands = PrinterFunctions.createTextReceiptData(StarIoExtEmulation.starPRNT, utf8: true)
//            }
//        }
//
//        var commandsArray: [UInt8] = [UInt8](repeating: 0, count: commands.count)
//
//        commands.copyBytes(to: &commandsArray, count: commandsArray.count)
//
//        while true {
//            var port : SMPort
//
//            do {
//                // Open port
//                port = try SMPort.getPort(portName: portName, portSettings: "", ioTimeoutMillis: 10000)
//
//                // Close port
//                defer {
//                    SMPort.release(port)
//                } // defer
//
//                var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
//
//                // Start to check the completion of printing
//                try port.beginCheckedBlock(starPrinterStatus: &printerStatus, level: 2)
//
//                if printerStatus.offline == sm_true {
//                    break    // The printer is offline.
//                }
//
//                var total: UInt32 = 0
//
//                while total < UInt32(commandsArray.count) {
//                    var written: UInt32 = 0
//
//                    // Send print data
//                    try port.write(writeBuffer: commandsArray, offset: total,
//                                size: UInt32(commandsArray.count) - total,
//                                numberOfBytesWritten: &written)
//
//                    total += written
//                } // while-loop
//
//                // Stop to check the completion of printing
//                try port.endCheckedBlock(starPrinterStatus: &printerStatus, level: 2)
//
//                if printerStatus.offline == sm_true {
//                    break    // The printer is offline.
//                }
//
//                // Success
//                break
//            }
//            catch let error as NSError {
//                break    // Some error occurred.
//            }
//        } // end while
//
//        self.removeSpinner()
//    }
//} // end print function
//
//
