//
//  SearchPortViewController.swift
//  SimpleApp
//  Created by Joshua Ventocilla on 9/26/22.
//

import UIKit

class SearchPortViewController: UIViewController {

    var portName:     String?
    var portSettings: String?
    var modelName:    String?
    var interfaceIndex: Int?
    
    // MARK: - PRINTER VARS
    @IBOutlet weak var printerModel: UILabel!
    @IBOutlet weak var printerPort: UILabel!
    @IBOutlet weak var printerSetting: UILabel!
    @IBOutlet weak var interfaceSegmentControl: UISegmentedControl!
    @IBOutlet weak var printButton: UIButton!
    
    var searchPrinterResult: [PortInfo]? = nil
    
    override func viewDidLoad() {
        
        // intialize the index
        interfaceIndex = 1
        
        // initialize layout
        printButton.isUserInteractionEnabled = false
        printButton.alpha                    = 0.5
        
        super.viewDidLoad()
    }
        
    @IBAction func selectInterface(_ sender: Any) {
        switch interfaceSegmentControl.selectedSegmentIndex {
        case 1:
            interfaceIndex = 1
        case 2:
            interfaceIndex = 2
        case 3:
            interfaceIndex = 3
        case 4:
            interfaceIndex = 4
        default:
            interfaceIndex = 0
        }
    }
    
    @IBAction func discoverPrinter(_ sender: UIButton) {
        self.showActivitySpinner()

        guard let interfaceIndex = interfaceIndex else {
            self.removeSpinner()
            return
        }

        didSelectRefreshPortInterface(buttonIndex: interfaceIndex)
    }
    
    @IBAction func testPrint(_ sender: UIButton) {
//         var commdandsArray: [UInt8] = [0x41, 0x42, 0x43, 0x44, 0x1b, 0x7a, 0x00, 0x1b, 0x64, 0x02]
        
        let commands: Data
        let emulation: StarIoExtEmulation = StarIoExtEmulation.starPRNT

//        commands = PrinterFunctions.createTextReceiptData(emulation,
//                                                          utf8: true)
        
        commands = PrinterFunctions.createTextReceiptData(StarIoExtEmulation.starPRNT, utf8: true, receiptType: 1)
        
        var commandsArray: [UInt8] = [UInt8](repeating: 0, count: commands.count)
        
        commands.copyBytes(to: &commandsArray, count: commandsArray.count)
        
        while true {
            var port : SMPort

            do {
            // Open port
            port = try SMPort.getPort(portName: portName, portSettings: "", ioTimeoutMillis: 10000)

            defer {
                // Close port
                SMPort.release(port)
            }

            var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
                
            // Start to check the completion of printing
            try port.beginCheckedBlock(starPrinterStatus: &printerStatus, level: 2)

            if printerStatus.offline == sm_true {
                break    // The printer is offline.
            }

            var total: UInt32 = 0

            while total < UInt32(commandsArray.count) {
                var written: UInt32 = 0

                // Send print data
                try port.write(writeBuffer: commandsArray, offset: total, size: UInt32(commandsArray.count) - total, numberOfBytesWritten: &written)

                total += written
            }

            // Stop to check the completion of printing
            try port.endCheckedBlock(starPrinterStatus: &printerStatus, level: 2)

            if printerStatus.offline == sm_true {
                break    // The printer is offline.
            }

            // Success
            break
            }
            catch let error as NSError {
            break    // Some error occurred.
            }
        }
    }
    
    func didSelectRefreshPortInterface(buttonIndex: Int) {
        
        do {
            switch buttonIndex {
            case 1:     // LAN
                searchPrinterResult = try SMPort.searchPrinter(target: "TCP:") as? [PortInfo]
            case 2:     // BLUETOOTH
                searchPrinterResult = try SMPort.searchPrinter(target: "BT:") as? [PortInfo]
            case 3:     // BLUETOOTH LOW ENERGY
                searchPrinterResult = try SMPort.searchPrinter(target: "BLE:") as? [PortInfo]
            case 4:     // USB
                searchPrinterResult = try SMPort.searchPrinter(target: "USB:") as? [PortInfo]
            default:    // ALL
                searchPrinterResult = try SMPort.searchPrinter(target: "ALL:") as? [PortInfo]
            }
        }
        catch {
            print("Some error occured")
        }

        guard let portInfoArray: [PortInfo] = searchPrinterResult else {
            return
        }
                        
        for portInfo: PortInfo in portInfoArray {
            printerModel.text   = portInfo.modelName
            printerPort.text    = portInfo.portName
            printerSetting.text = portInfo.macAddress
            portName            = portInfo.portName
        }
        
        self.removeSpinner()
        printButton.isUserInteractionEnabled = true
        printButton.alpha = 1.0
    
    }
    
}
