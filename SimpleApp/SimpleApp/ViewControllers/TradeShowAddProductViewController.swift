//
//  TradeShowAddProductViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/17/22.
//

import UIKit

/*
    Product object for items added
    @item = the name of the model to be added
 */
struct AddedProduct: Codable {
    var item: String
    var added: Bool
}

class TradeShowAddProductViewController: UIViewController {
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var isDone: Bool = false
    
    var currentProducts: [AddedProduct] = []
    var currentImages: [String]         = []
    var currentType: [String]           = []
        
    // a copy of current products added
    var addedProducts: [String]         = []
    var addedProductsType: [String]     = []
    var addedProductsImage: [String]    = []
    
    var categoryID: Int?
    var products: Category?
    var list: [Model]?
    
    @IBOutlet weak var listTableView: UITableView!
    
    @IBOutlet weak var categoryLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listTableView.register(ProductAddTableViewCell.nib(), forCellReuseIdentifier: ProductAddTableViewCell.identifier)
        
        listTableView.delegate   = self
        listTableView.dataSource = self
        
        isDone = false
        
        if let savedProducts = UserDefaults.standard.object(forKey: "savedItems") {
            addedProducts = savedProducts as! [String]
        }
        
        if let savedType = UserDefaults.standard.object(forKey: "savedItemsType") {
            addedProductsType = savedType as! [String]
        }
        
        if let savedImages = UserDefaults.standard.object(forKey: "savedItemsImage") {
            addedProductsImage = savedImages as! [String]
        }
                
        parseJSON {
            checkForProductsAdded()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UserDefaults.standard.set(addedProducts, forKey: "savedItems")
        UserDefaults.standard.set(addedProductsType, forKey: "savedItemsType")
        UserDefaults.standard.set(addedProductsImage, forKey: "savedItemsImage")
        
        if isDone {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        isDone = true
        navigationController?.popViewController(animated: true)
    }
    
    /*
        Compare products that have been added and make sure to change button to indicate the product has been added or not
     */
    func checkForProductsAdded() {
        var index = 0
        for product in currentProducts {
            for saved in addedProducts {
                if product.item == saved {
                    currentProducts[index].added = true
                }
            }
            index += 1  // increment for next product
        }
    }
    
    func saveCurrentPorts() {
        let savedPorts = currentPorts
        if let data = try? PropertyListEncoder().encode(savedPorts) {
            UserDefaults.standard.set(data, forKey: "currentlySavedPorts")
        }
    }
        
    func parseJSON(completion: () -> Void) {

        guard let path = Bundle.main.path(forResource: "category", ofType: "json") else {
            return
        }
        
        let url = URL(fileURLWithPath: path)
        
        do {
            let data = try Data(contentsOf: url)
            products = try JSONDecoder().decode(Category.self, from: data)
        } catch {
            // Alert user that data was unable to load
            print("Error: \(error)")
        }
                
        switch categoryID {
            case 0:
                list = products?.printers
                categoryLabel.text = "Printers"
            case 1:
                list = products?.cashdrawers
                categoryLabel.text = "Cash Drawers"
            case 2:
                list = products?.scales
                categoryLabel.text = "Scales"
            case 3:
                list = products?.stands
                categoryLabel.text = "Stands"
            case 4:
                list = products?.scanners
                categoryLabel.text = "Scanners"
            default:
                categoryLabel.text = "Label"
        }
        
        for models in list ?? [] {
            currentProducts.append(AddedProduct(item: models.item,
                                                added: false))
            currentImages.append(models.image)
            currentType.append(models.type)
        }
        
        completion()
    }
    
}

extension TradeShowAddProductViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("just tapped")
    }
    
}

extension TradeShowAddProductViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProductAddTableViewCell.identifier, for: indexPath) as! ProductAddTableViewCell
        
        cell.cellDelegate  = self
        cell.configureCell(item: currentProducts[indexPath.row].item,
                           image: currentImages[indexPath.row],
                           type: currentType[indexPath.row],
                           index: indexPath.row,
                           added: currentProducts[indexPath.row].added)
        
        cell.selectionStyle = .none
        
        return cell
    }
    
}

extension TradeShowAddProductViewController: ProductAddTableViewDelegate {
    
    func didTapAddButton(_ cell: ProductAddTableViewCell) {
        if let currentPosition = cell.cellIndex {
            
            // adding product
            if currentProducts[currentPosition].added == false {
                currentProducts[currentPosition].added = true
            
                addedProducts.append(currentProducts[currentPosition].item)
                addedProductsType.append(currentType[currentPosition])
                addedProductsImage.append(currentImages[currentPosition])
                
                // change cell ui
                cell.addButton.backgroundColor = UIColor.red
                cell.addButton.setTitle("Remove", for: .normal)
            } else {

                let itemToRemove = currentProducts[currentPosition].item
                
                // delete product
                var index = 0
                for item in addedProducts {
                    // remove from added products
                    if itemToRemove == item {
                        addedProducts.remove(at: index)
                        addedProductsType.remove(at: index)
                        addedProductsImage.remove(at: index)
                    }
                    index += 1
                }
                
                // delete from saved ports if its been saved before
                index = 0
                if !currentPorts.isEmpty {
                    for port in currentPorts {
                        if port.name == itemToRemove {
                            currentPorts.remove(at: index)
                        }
                        index += 1
                    }
                }
                
                // change cell ui and status
                currentProducts[currentPosition].added = false
                cell.addButton.backgroundColor         = UIColor.systemBlue
                cell.addButton.setTitle("Add", for: .normal)
                
                saveCurrentPorts()
            }
        }
                
    }
    
}
