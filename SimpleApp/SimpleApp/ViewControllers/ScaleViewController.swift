//
//  ScaleViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 1/3/23.
//

import Foundation

/*
    Class for Scale screen. This is where we should discover the scales and devices and show them in table view.
 */
class ScaleViewController: UIViewController {
   
    var centralManager: CBCentralManager!
    var connectedScale: STARScale? = nil
    var scales: [STARScale]        = []
    
    @IBOutlet weak var scalesTableView: UITableView!
    
    // MARK: - VIEW LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scalesTableView.register(SearchPortTableViewCell.nib(), forCellReuseIdentifier: SearchPortTableViewCell.identifier)
        scalesTableView.delegate            = self
        scalesTableView.dataSource          = self
        STARDeviceManager.shared().delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Initialize CBCentral manager. This will/should begin to call delegates..
        centralManager = CBCentralManager(delegate: self, queue: nil)

        // gets disconnected here after leaving view... possibly use a singleton class?
        if (centralManager?.state == .poweredOn) {
            STARDeviceManager.shared().scanForScales()
        }
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // NOTE: Stop scan must be done when leaving otherwise when view is loaded again it will leave
        //      an empty scan. On Star scale SDK this is on cellForIndex at, but since this is more than
        //      a single view application this needs to be in viewWillDisappear
        STARDeviceManager.shared().stopScan()
        resetData()
        DispatchQueue.main.async {
            self.scalesTableView.reloadData()
        }
    }
    
    // MARK: - FUNCTIONS
    func resetData() {
        scales.removeAll()
    }
    
}

//MARK: - STARDeviceManagerDelegate
extension ScaleViewController: STARDeviceManagerDelegate {
    func manager(_ manager: STARDeviceManager, didDiscover scale: STARScale?, error: Error?) {
        if (error != nil) {
            alertFailedConnection()
            return
        }
        
        // discovered and populate 
        scales.append(scale!)
        scalesTableView.reloadData()
    }
        
    func manager(_ manager: STARDeviceManager, didConnect scale: STARScale?, error: Error?) {
        if (error != nil) {
            alertFailedConnection()
            return
        }
        
        let scannerSB = UIStoryboard.init(name: "Scale", bundle: nil)
        let scannerVC = scannerSB.instantiateViewController(withIdentifier: "ScaleOutput") as!
            ScaleOutputViewController
        
        scannerVC.scale = scale
        scannerVC.scale?.delegate = scannerVC
        
        scannerVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(scannerVC, animated: true)
        
    }
    
    func manager(_ manager: STARDeviceManager, didDisconnectScale scale: STARScale?, error: Error?) {
        print("Did Disconnect")
    }
}

//MARK: - UITableView Delegate
extension ScaleViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected: STARScale = scales[indexPath.row]
        STARDeviceManager.shared().connect(selected)
    }
}

extension ScaleViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        scales.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchPortTableViewCell.identifier, for: indexPath) as! SearchPortTableViewCell
        
        let row = indexPath.row
        
        if let name = scales[row].name {
            if let scaleID = scales[row].identifier {
                cell.configureCell(model: name,
                                   portName: scaleID,
                                   macAddress: "")
            }
        }
        
        return cell
    }
}

//MARK: - CBCentralManagerDelegate

/*
    Used for Bluetooth Low Energy. Called when bluetooth is on and are connecting to devices or to check status
 */
extension ScaleViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        // clear scales for reset and find
        resetData()
        
        switch central.state {
        case .poweredOn:
            STARDeviceManager.shared().scanForScales()
            break;
        default:
            STARDeviceManager.shared().stopScan()
            
            resetData()
            DispatchQueue.main.async {
                self.scalesTableView.reloadData()
            }
            
            break
        }
    }
}
