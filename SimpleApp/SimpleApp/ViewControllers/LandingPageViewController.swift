//
//  LandingPageViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/8/22.
//

import UIKit

class LandingPageViewController: UIViewController {
    
    @IBOutlet weak var starLogoImageView: UIImageView!
    @IBOutlet weak var starMotoImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.animateLogo(imageView: self.starLogoImageView, duration: 1.0, scaleX: 0.1, scaleY: 0.1)
        })
    }
    
    // Animation after loading
    func animateLogo(imageView: UIImageView, duration: TimeInterval,
                     scaleX: CGFloat, scaleY: CGFloat) {
        UIImageView.animate(withDuration: duration, animations: {
            
            self.starLogoImageView.transform = CGAffineTransformMakeScale(scaleX, scaleY);
            self.starLogoImageView.alpha = 0.0
            self.starMotoImageView.alpha = 0.0
           
        }, completion: { done in
            if done {
                let homeViewSB = UIStoryboard(name: "HomeView", bundle: nil)
                let homeViewController = homeViewSB.instantiateViewController(withIdentifier: "HomeView") as! HomeViewController
                
                let rootNC = UINavigationController(rootViewController: homeViewController)
                UIApplication.shared.windows.first?.rootViewController = rootNC
                UIApplication.shared.windows.first?.makeKeyAndVisible()
                                
                homeViewController.modalTransitionStyle = .crossDissolve
                homeViewController.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(homeViewController, animated: true)

            }
        })
    }
    
}
