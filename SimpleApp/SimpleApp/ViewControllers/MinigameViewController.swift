//
//  MinigameViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 12/11/22.
//

import UIKit
import AVFoundation

class MinigameViewController: UIViewController {
    
    let sound: SoundManager = SoundManager()
    var audioPlayer: AVAudioPlayer?
    
    //MARK: - IBOUTLET
    @IBOutlet weak var spinButton: UIButton!
    @IBOutlet weak var exitButton: UIBarButtonItem!
    
    @IBOutlet weak var numberLabel: UILabel!
    //MARK: - VARS / LETS
    var printer: String          = ""
    var printerMac: String       = ""
    var printerPort: String      = ""
    
    //MARK: - VIEW LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
                
        view.addBackground(imageName: "", contentMode: .scaleAspectFit)
        
        let width = view.frame.size.width
        let height = view.frame.size.height
        
        // layout for device orientation
        guard let imageView = view.viewWithTag(1) as? UIImageView else {
            return
        }
        
        if UIDevice.current.orientation.isFlat {
            // device is flat and in LANDSCAPE,
            if width > height {
                imageView.image = UIImage(named: "banner-minigame-landscape")
            } else {
            // device is flat and in PORTRAIT
                imageView.image = UIImage(named: "banner-portrait-2")
            }
        } else {
            // device is in LANDSCAPE (width > height) || device is in PORTRAIT (height > width)
            if width > height {
                imageView.image = UIImage(named: "banner-minigame-landscape")
            } else {
            // device is in PORTRAIT
                imageView.image = UIImage(named: "banner-portrait-2")
            }
        
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        printer     = ""
        printerMac  = ""
        printerPort = ""
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        guard let imageView = view.viewWithTag(1) as? UIImageView else {
            return
        }
        
        let width = view.frame.size.width
        let height = view.frame.size.height

        if UIDevice.current.orientation.isFlat {
            // device is flat and in LANDSCAPE,
            if width > height {
                imageView.image = UIImage(named: "banner-minigame-landscape")
            } else {
            // device is flat and in PORTRAIT
                imageView.image = UIImage(named: "banner-portrait-2")
            }
        } else {
            // device is in LANDSCAPE (width > height) || device is in PORTRAIT (height > width)
            if width > height {
                // imageView.image = UIImage(named: "banner-minigame-landscape")
                imageView.image = UIImage(named: "banner-portrait-2")
            } else {
            // device is in PORTRAIT
                imageView.image = UIImage(named: "banner-minigame-landscape")
                // imageView.image = UIImage(named: "banner-portrait-2")
            }
        }
    }
    
    //MARK: - FUNCTIONS
    func animateButton(completion: @escaping() -> Void) {
        
        DispatchQueue.main.async {
            self.showActivitySpinner()
            self.disableSpinButton()
        }
        
        // Animate button
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            // play button increase
            self.spinButton.bounds.size.width += 10
        }, completion: { finished in
            completion()
        })
    }
    
    func enableSpinButton() {
        spinButton.isUserInteractionEnabled = true
        spinButton.alpha = 1.0
    }
    
    func disableSpinButton() {
        spinButton.isUserInteractionEnabled = false
        spinButton.alpha = 0.5
    }
    
    func generateRandomNumber(min: Int, max: Int) -> Int {
        let randomNumber = Int.random(in: min..<max)
        return randomNumber
    }
    
    func printRandomNumber(model: String, port: String, mac: String, prize: String) {
    
        let emulation: StarIoExtEmulation = StarIoExtEmulation.starPRNT
        let builder: ISCBBuilder          = StarIoExt.createCommandBuilder(emulation)
        let bitmapImage                   = UIImage(named: "star-logo-black")
        
        let fontWidth: Int  = 20
        let fontHeight: Int = 20
        
        builder.beginDocument()
        // start
        
        builder.append(SCBCodePageType.UTF8)
        builder.append(SCBInternationalType.USA)
        builder.appendCharacterSpace(0)
        
        // star logo
        builder.appendBitmap(withAlignment: bitmapImage, diffusion: true, width: 300, bothScale: true,
                             rotation: .normal, position: .center)
        builder.appendAlignment(SCBAlignmentPosition.center)
        
        // append random number
        builder.appendMultiple(2, height: 2)
        builder.appendAlignment(.center)
        builder.append(("\n\n \(prize) \n\n").data(using: String.Encoding.utf8))

        builder.appendMultiple(1, height: 1)
        builder.appendAlignment(.center)
        builder.append(("Thanks for playing and stopping by!\n").data(using: String.Encoding.utf8))
        builder.append(("Want to learn more about Star Micronics?\n" +
                        "Scan the QR code below.\n\n").data(using: String.Encoding.utf8))
        
        // qr code
        builder.appendAlignment(SCBAlignmentPosition.center)
        builder.appendQrCodeData(withAlignment: "www.starmicronics.com".data(using: String.Encoding.ascii),
                                 model: SCBQrCodeModel.no2,
                                 level: SCBQrCodeLevel.M,
                                 cell: 5,
                                 position: SCBAlignmentPosition.center)
        
        builder.appendAlignment(.center)
        builder.append(("\nwww.starmicronics.com\n").data(using: String.Encoding.utf8))
        
        builder.appendCutPaper(SCBCutPaperAction.fullCutWithFeed)
        
        // end
        builder.endDocument()

        let command = builder.commands.copy() as! Data
                        
        var commandsArray: [UInt8] = [UInt8](repeating: 0, count: command.count)
                        
        command.copyBytes(to: &commandsArray, count: commandsArray.count)
                        
        while true {
            var port : SMPort

            do {
                // Open port
                port = try SMPort.getPort(portName: printerPort, portSettings: "", ioTimeoutMillis: 10000)

                // Close port
                defer {
                    SMPort.release(port)
                } // defer

                var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()

                // Start to check the completion of printing
                try port.beginCheckedBlock(starPrinterStatus: &printerStatus, level: 2)

                if printerStatus.offline == sm_true {
                    break    // The printer is offline.
                }

                var total: UInt32 = 0

                while total < UInt32(commandsArray.count) {
                    var written: UInt32 = 0

                    // Send print data
                    try port.write(writeBuffer: commandsArray, offset: total,
                                size: UInt32(commandsArray.count) - total,
                                numberOfBytesWritten: &written)

                    total += written
                } // while-loop

                // Stop to check the completion of printing
                try port.endCheckedBlock(starPrinterStatus: &printerStatus, level: 2)

                if printerStatus.offline == sm_true {
                    break    // The printer is offline.
                }

                // Success
                break
                }
                catch let error as NSError {
                    break    // Some error occurred.
            }
        } // end print
    }

    func randomize() {
        
        var randomNumber: Int = 0
        
        // Animate numbers
        UIView.transition(with: self.numberLabel, duration: 2.0, options: .transitionCrossDissolve, animations: { () -> Void in
            // do something if need be
        }) { (value: Bool) -> Void in
            self.removeSpinner()
            DispatchQueue.global().async {
                for i in 1...99 {
                    let sleepTime = UInt32(2.0/Double(99) * 1000000.0)
                    usleep(sleepTime)
                                   
                    DispatchQueue.main.async {
                        randomNumber = Int(arc4random_uniform(60) + 1)
                        self.numberLabel.text = self.prizeOutcome(number: randomNumber)
                    }
                                   
                    if i == 99 {
                        SoundManager.instance.play(resource: "tada", fileFormat: "mp3")
                        
                        DispatchQueue.main.async {
                            self.showConfetti()
                        }
                        
                        self.printRandomNumber(model: self.printer,
                                               port: self.printerPort,
                                               mac: self.printerMac,
                                               prize: self.prizeOutcome(number: randomNumber) )
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 7.0) {
                            self.removeConfetti()
                            self.enableSpinButton()
                        }
                    }
                }
            }
        }
    }
    
    func showConfetti() {
        let layer = CAEmitterLayer()
        layer.emitterPosition = CGPoint(x: view.center.x, y: -100)
        
        let colors: [UIColor] = [
            .systemRed,
            .systemBlue,
            .systemOrange,
            .systemGreen,
            .systemGreen,
            .systemPink,
            .systemYellow,
            .systemPurple
        ]
        
        let cells: [CAEmitterCell] = colors.compactMap {
            let cell = CAEmitterCell()
            cell.scale = 0.1
            cell.emissionRange = .pi * 2
            cell.lifetime = 20
            cell.birthRate = 50
            cell.velocity = 125
            cell.color    = $0.cgColor
            cell.contents = UIImage(named: "white-square")!.cgImage
            return cell
        }

        layer.emitterCells = cells
        view.layer.addSublayer(layer)
    }
    
    func removeConfetti() {
        self.view.layer.sublayers?.popLast()
    }
    
    func prizeOutcome(number: Int) -> String {
        var prize = ""
        
        if (number >= 1 && number <= 13) {
            prize = "SURPRISE!"
        } else if (number >= 14 && number <= 30) {
            prize = "MEASURING TAPE"
        } else if (number >= 31 && number <= 41) {
            prize = "CUP"
        } else if (number >= 52 && number <= 70) {
            prize = "SQUISHY PRINTER"
        } else if (number >= 71 && number <= 84) {
            prize = "KOOZY"
        } else {
            prize = "PEN"
        }
        
        return prize
    }
    
    //MARK: - IBAction
    @IBAction func randomizeNumber(_ sender: Any) {
        if printerPort.isEmpty {
            alertNoDevicesFound()
        } else {
            animateButton() {
                self.randomize()
            }
        }
    }
    
    @IBAction func exitMinigame(_ sender: Any) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
    
    @IBAction func searchPrinter(_ sender: Any) {
        let searchPortSB = UIStoryboard.init(name: "Search", bundle: nil)
        let searchPortVC = searchPortSB.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
                                
        searchPortVC.searchViewDelegate     = self
        searchPortVC.isMinigame             = true
        searchPortVC.modalTransitionStyle   = .flipHorizontal
        searchPortVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(searchPortVC, animated: true)
    }
}

// MARK: - DELEGATE
extension MinigameViewController: SearchViewDelegate {
    func printerInformation(model: String, port: String, macAddress: String, index: Int) {
        
        // get information
        printer = model
        printerMac = macAddress
        printerPort = port
        
        let commands: Data
        let emulation: StarIoExtEmulation = StarIoExtEmulation.starPRNT

        commands = PrinterFunctions.createConnectionSuccessfulTestReceiptData(emulation, utf8: true,
                                                                              modelName: model,
                                                                              portName: printerPort,
                                                                              macAddress: macAddress)
                        
        var commandsArray: [UInt8] = [UInt8](repeating: 0, count: commands.count)
                        
        commands.copyBytes(to: &commandsArray, count: commandsArray.count)
                        
        while true {
            var port : SMPort

            do {
                // Open port
                port = try SMPort.getPort(portName: printerPort, portSettings: "", ioTimeoutMillis: 10000)

                // Close port
                defer {
                    SMPort.release(port)
                } // defer

                var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()

                // Start to check the completion of printing
                try port.beginCheckedBlock(starPrinterStatus: &printerStatus, level: 2)

                if printerStatus.offline == sm_true {
                    break    // The printer is offline.
                }

                var total: UInt32 = 0

                while total < UInt32(commandsArray.count) {
                    var written: UInt32 = 0

                    // Send print data
                    try port.write(writeBuffer: commandsArray, offset: total,
                                size: UInt32(commandsArray.count) - total,
                                numberOfBytesWritten: &written)

                    total += written
                } // while-loop

                // Stop to check the completion of printing
                try port.endCheckedBlock(starPrinterStatus: &printerStatus, level: 2)

                if printerStatus.offline == sm_true {
                    break    // The printer is offline.
                }

                // Success
                break
                }
                catch let error as NSError {
                    break    // Some error occurred.
            }
        } // end print
    }
}
