//
//  DeviceStatusViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 2/28/24.
//

import Foundation

class DeviceStatusViewController: UIViewController {
    
    @IBOutlet weak var deviceStatusLabel: UILabel!
    @IBOutlet weak var firmwareLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var externalDeviceButton1: UIButton!
    @IBOutlet weak var externalDeviceButton2: UIButton!
    
    @IBOutlet weak var statusTableView: UITableView!
    
    let model = PrinterModel()
    var timer = Timer()
        
    var printerName: String?
    var printerPort: String?
    var status: [Status]     = []

    override func viewDidLoad() {
        super.viewDidLoad()
                        
        statusTableView.register(DeviceStatusTableViewCell.nib(), forCellReuseIdentifier: DeviceStatusTableViewCell.identifier)
                
        statusTableView.delegate   = self
        statusTableView.dataSource = self
                
        if printerName == model.mpop {          // since mPop is a cashdrawer/printer combo
            enableExternalDeviceButton1()
            disableExternalDeviceButton2()
        } else {
            disableExternalDeviceButton1()
            disableExternalDeviceButton2()
        }
        
        checkStatus()
            
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { _ in
            self.reset()
            self.checkStatus()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    func reset() {
        status.removeAll()
    }
    
    func disableExternalDeviceButton1() {
        externalDeviceButton1.isEnabled = false
        externalDeviceButton1.backgroundColor = UIColor.systemGray4
        externalDeviceButton1.tintColor       = UIColor.white
        externalDeviceButton1.alpha           = 0.4
    }
    
    func disableExternalDeviceButton2() {
        externalDeviceButton2.isEnabled = false
        externalDeviceButton2.backgroundColor = UIColor.systemGray4
        externalDeviceButton2.tintColor       = UIColor.white
        externalDeviceButton2.alpha           = 0.4
    }
    
    func enableExternalDeviceButton1() {
        externalDeviceButton1.isEnabled = true
        externalDeviceButton1.alpha                    = 1.0
        externalDeviceButton1.backgroundColor          = UIColor(named: "star-blue")
        externalDeviceButton1.tintColor       = UIColor.white
    }
    
    func enableExternalDeviceButton2() {
        externalDeviceButton2.isEnabled = true
        externalDeviceButton2.alpha                    = 1.0
        externalDeviceButton2.backgroundColor          = UIColor(named: "star-blue")
        externalDeviceButton2.tintColor       = UIColor.white
    }
        
    @IBAction func tapExternalDevice1(_ sender: Any) {
        
        let commands: Data
 
        while true {
            var port : SMPort
            
            do {
                // Open port
                port = try SMPort.getPort(portName: printerPort, portSettings: "", ioTimeoutMillis: 10000)
                
                // Close port
                defer {
                    SMPort.release(port)
                } // defer

                var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
                
                try port.getParsedStatus(starPrinterStatus: &printerStatus, level: 2)

                if printerStatus.offline == sm_true {
                    break // offline
                }
                
                commands = PrinterFunctions.triggerPeripheral(StarIoExtEmulation.starPRNT, utf8: true, channel: SCBPeripheralChannel.no1)

                var commandsArray: [UInt8] = [UInt8](repeating: 0, count: commands.count)
                
                commands.copyBytes(to: &commandsArray, count: commandsArray.count)
                
                var total: UInt32 = 0

                while total < UInt32(commandsArray.count) {
                    var written: UInt32 = 0

                    // Send print data
                    try port.write(writeBuffer: commandsArray, offset: total,
                                size: UInt32(commandsArray.count) - total,
                                numberOfBytesWritten: &written)

                    total += written
                } // while-loop

                if printerStatus.offline == sm_true {
                    break    // The printer is offline.
                }

                // Success
                break
            }
            catch let error as NSError {
                break    // Some error occurred.
            }
            
        } // end while
    }
    
    @IBAction func tapExternalDevice2(_ sender: Any) {
        let commands: Data
        
        while true {
            var port : SMPort
            
            do {
                // Open port
                port = try SMPort.getPort(portName: printerPort, portSettings: "", ioTimeoutMillis: 10000)
                
                // Close port
                defer {
                    SMPort.release(port)
                } // defer

                var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
                
                try port.getParsedStatus(starPrinterStatus: &printerStatus, level: 2)

                if printerStatus.offline == sm_true {
                    break // offline
                }
                
                commands = PrinterFunctions.triggerPeripheral(StarIoExtEmulation.starPRNT, utf8: true, channel: SCBPeripheralChannel.no2)
                
                var commandsArray: [UInt8] = [UInt8](repeating: 0, count: commands.count)
                
                commands.copyBytes(to: &commandsArray, count: commandsArray.count)
                
                var total: UInt32 = 0

                while total < UInt32(commandsArray.count) {
                    var written: UInt32 = 0

                    // Send print data
                    try port.write(writeBuffer: commandsArray, offset: total,
                                size: UInt32(commandsArray.count) - total,
                                numberOfBytesWritten: &written)

                    total += written
                } // while-loop

                if printerStatus.offline == sm_true {
                    break    // The printer is offline.
                }

                // Success
                break
            }
            catch let error as NSError {
                break    // Some error occurred.
            }
            
        } // end while
    }
    
    func checkStatus() {
        while true {
            var port : SMPort
            
            do {
                // Open port
                port = try SMPort.getPort(portName: printerPort, portSettings: "", ioTimeoutMillis: 10000)

                // Close port
                defer {
                    SMPort.release(port)
                } // defer

                var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
                
                try port.getParsedStatus(starPrinterStatus: &printerStatus, level: 2)

                if printerStatus.offline == sm_true {
                    statusLabel.text = "Offline"
                    statusLabel.textColor = .red
                    
                    // offline so devices are offline too
                    disableExternalDeviceButton1()
                    disableExternalDeviceButton2()
                } else {
                    let dictionary = try port.getFirmwareInformation()
                    
                    let modelName: String = dictionary["ModelName"] as! String
//                    let firmware: String  = dictionary["FirmwareVersion"] as! String
                    
                    nameLabel.text = printerName
                    statusLabel.text = "Online"
                    statusLabel.textColor = .systemGreen
                }
                
                if (printerName == PrinterModel().mclabel || printerName == PrinterModel().tsp100iv || printerName == PrinterModel().tsp100ivsk ||
                    printerName == PrinterModel().mcp30 || printerName == PrinterModel().mcp31) {
                    
                    if printerStatus.drawer1OpenCloseSignal == sm_true || printerStatus.drawer2OpenCloseSignal == sm_true {
                        if printerName == model.sm_s230i || printerName == model.sm_t300 || printerName == model.sm_t400 {
                            // do nothing
                        } else {
                            status.append(Status(status: "Open", statusType: "Cash Drawer Status", state: 0))
                        }
                    } else {
                        if printerName == model.sm_s230i || printerName == model.sm_t300 || printerName == model.sm_t400 {
                            // do nothing
                        } else {
                            status.append(Status(status: "Closed", statusType: "Cash Drawer Status", state: 1))
                        }
                    }
                                        
                    if printerStatus.externalDevice1Connected == sm_true {
                        status.append(Status(status: "Connected", statusType: "External Device 1", state: 1))
                        enableExternalDeviceButton1()
                        
                        if printerStatus.compulsionSwitch == 1 {
                            if printerStatus.drawer2OpenedMethod == 1 {
                                status.append(Status(status: "Command", statusType: "Drawer 1 Open Method", state: 3))
                            } else {
                                status.append(Status(status: "Manual", statusType: "Drawer 1 Open Method", state: 3))
                            }
                        } else {
                            status.append(Status(status: "Unknown", statusType: "Drawer 1 Open Method", state: 3))
                        }
                                                
                    } else {
                        status.append(Status(status: "Disconnected", statusType: "External Device 1", state: 0))
                        status.append(Status(status: "Unknown", statusType: "Drawer 1 Open Method", state: 3))
                        disableExternalDeviceButton1()
                    }
                    
                    if printerStatus.externalDevice2Connected == sm_true {
                        status.append(Status(status: "Connected", statusType: "External Device 2", state: 1))
                        enableExternalDeviceButton2()
                        
                        if printerStatus.compulsionSwitch == 1 {
                            if printerStatus.drawer2OpenedMethod == 1 {
                                status.append(Status(status: "Command", statusType: "Drawer 2 Open Method", state: 3))
                            } else {
                                status.append(Status(status: "Manual", statusType: "Drawer 2 Open Method", state: 3))
                            }
                        } else {
                            status.append(Status(status: "Unknown", statusType: "Drawer 2 Open Method", state: 3))
                        }
                        
                    } else {
                        status.append(Status(status: "Disconnected", statusType: "External Device 2", state: 0))
                        status.append(Status(status: "Unknown", statusType: "Drawer 2 Open Method", state: 3))
                        disableExternalDeviceButton2()
                    }
                    
//                    if printerStatus.connectedInterface == usb_b {
//                        status.append(Status(status: "USB-B", statusType: "Connected Interface", state: 1))
//                    } else if printerStatus.connectedInterface == bt {
//                        status.append(Status(status: "Bluetooth", statusType: "Connected Interface", state: 1))
//                    } else if printerStatus.connectedInterface == ethernet {
//                        status.append(Status(status: "Ethernet", statusType: "Connected Interface", state: 1))
//                    } else if printerStatus.connectedInterface == usb_c_pd_support {
//                        status.append(Status(status: "USB-C (USB PD Support)", statusType: "Connected Interface", state: 1))
//                    } else if printerStatus.connectedInterface == usb_c_pd_nosupport {
//                        status.append(Status(status: "USB-C (USB PD No Support)", statusType: "Connected Interface", state: 1))
//                    } else {
//                        status.append(Status(status: "USB (iOS)", statusType: "Connected Interface", state: 1))
//                    }
                    
                    if printerStatus.coverOpen == sm_true {
                        status.append(Status(status: "Open", statusType: "Cover", state: 0))
                    } else {
                        status.append(Status(status: "Closed", statusType: "Cover", state: 1))
                    }
                    
                    if printerName == PrinterModel().mclabel {
                        if printerStatus.detectedPaperWidth == paper_25mm {
                            status.append(Status(status: "25mm", statusType: "Paper Width", state: 1))
                        } else if printerStatus.detectedPaperWidth == paper_39mm {
                            status.append(Status(status: "39mm", statusType: "Paper Width", state: 1))
                        } else if printerStatus.detectedPaperWidth == paper_58mm {
                            status.append(Status(status: "58mm", statusType: "Paper Width", state: 1))
                        } else {
                            status.append(Status(status: "80mm", statusType: "Paper Width", state: 1))
                        }
                        
//                        if printerStatus.rollPositionError == 0 {
//                            status.append(Status(status: "Loaded", statusType: "Roll Position", state: 1))
//                        } else {
//                            status.append(Status(status: "Check Roll Position", statusType: "Roll Position", state: 0))
//                        }
                        
                        if printerStatus.paperPresent == 0 {
                            status.append(Status(status: "Paper Not Present", statusType: "Paper Present", state: 1))
                        } else {
                            status.append(Status(status: "Paper Present", statusType: "Paper Present", state: 0))

                        }
                    }
                    
                    if printerName == PrinterModel().tsp100ivsk {
                        if printerStatus.paperPresent == 0 {
                            status.append(Status(status: "Paper Not Present", statusType: "Paper Present", state: 1))
                        } else {
                            status.append(Status(status: "Paper Hold", statusType: "Paper Present", state: 0))

                        }
                    }
                    
                }
                                                
//                if printerStatus.compulsionSwitch == sm_true {
//                    // cannot have cash drawers
//                    if printerName == model.sm_s230i || printerName == model.sm_t300 || printerName == model.sm_t400 {
//                        // do nothing
//                    } else {
//                        status.append(Status(status: "Pressed", statusType: "Compulsion Switch", state: 0))
//                    }
//                    
//                } else {
//                    
//                    // cannot have cash drawers
//                    if printerName == model.sm_s230i || printerName == model.sm_t300 || printerName == model.sm_t400 {
//                        // do nothing
//                    } else {
//                        status.append(Status(status: "Not Pressed", statusType: "Compulsion Switch", state: 1))
//                    }
//                    
//                }
                
//                if printerStatus.cutterError == sm_true {
//                    status.append(Status(status: "Error", statusType: "Cutter", state: 0))
//                } else {
//                    status.append(Status(status: "Ready", statusType: "Cutter", state: 1))
//                }
                
                if printerStatus.receiptPaperEmpty == sm_true {
                    status.append(Status(status: "Paper End", statusType: "Paper", state: 1))
                } else if printerStatus.receiptPaperNearEmptyInner == sm_true
                            || printerStatus.receiptPaperNearEmptyOuter == sm_true {
                    status.append(Status(status: "Paper Near End", statusType: "Paper", state: 1))
                } else {
                    status.append(Status(status: "Paper Ready", statusType: "Paper", state: 0))
                }
                
                // Success
                break
            }
            catch let error as NSError {
                statusLabel.text = "Offline"
                statusLabel.textColor = .red
                break    // Some error occurred.
            }
        } // end while
        
        statusTableView.reloadData()
    }
    
}

extension DeviceStatusViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return status.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: DeviceStatusTableViewCell.identifier, for: indexPath) as! DeviceStatusTableViewCell
        let cellIndex = indexPath.row
        
        cell.selectionStyle = .none
        cell.configureCell(statusArray: status[cellIndex])
                
        return cell
    }
    
}
