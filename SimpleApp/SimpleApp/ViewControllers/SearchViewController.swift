//
//  SearchViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/27/22.
//

import UIKit

// struct for saved printer information
struct PrinterInfo: Codable {
    var name: String
    var port: String
    var mac: String
    var connected: Bool
}

protocol SearchViewDelegate: AnyObject {
    func printerInformation(model: String, port: String, macAddress: String, index: Int)
}

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchedPrintersTableView: UITableView!
    
    @IBOutlet weak var retryDiscoveryBarButton: UIBarButtonItem!
    
    weak var searchViewDelegate: SearchViewDelegate?
    
    var searchResult: [PortInfo]?         = nil
    var isMinigame: Bool                  = false
    var printerPort: [String]             = []
    var printerMac: [String]              = []
    var printerModel: [String]            = []
    var printerIndex: Int?
    var currentPrinter: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchedPrintersTableView.register(SearchPortTableViewCell.nib(),
                                           forCellReuseIdentifier: SearchPortTableViewCell.identifier)
        
        searchedPrintersTableView.dataSource = self
        searchedPrintersTableView.delegate   = self
        
        showDiscoverySpinner()
            
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.didSelectRefreshPortInterface(interface: "All:") { modelName, portName, portMac in
           
            self.removeSpinner()
            self.searchedPrintersTableView.reloadData()
            
            if self.printerModel.count == 0 {
                self.alertNoDevicesFound()
            }
    
        }
    }
    
    func resetPortInfo() {
        printerPort.removeAll()
        printerModel.removeAll()
        printerMac.removeAll()
    }
    
    func selectInterface(completion: @escaping(String) -> Void) {
        var dialogueMessage = UIAlertController(title: "No Devices Found", message: "Would you like to try finding available devices?", preferredStyle: .alert)
        
        let all = UIAlertAction(title: "Retry", style: .default, handler: { (action) -> Void in
            completion("ALL:")
        })
        
        let manual = UIAlertAction(title: "Manual", style: .default, handler: { (action) -> Void in
            self.showManualInterface()
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        })
        
        dialogueMessage.addAction(all)
        dialogueMessage.addAction(cancel)
        
        self.present(dialogueMessage, animated: true, completion: nil)
    }
    
    func showManualInterface() {
        let manualInput = UIAlertController(title: "Select Interface", message: "", preferredStyle: .alert)
        manualInput.addTextField()
        
        let input = UIAlertAction(title: "OK", style: .default) { [unowned manualInput] _ in
            let answer = manualInput.textFields![0]
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in

        })
        
        manualInput.addAction(input)
        manualInput.addAction(cancel)
        
        self.present(manualInput, animated: true, completion: nil)
    }
    
    func showSelectedPrinter(selected: String, index: Int) {
        let confirm = UIAlertController(title: "Confirm", message: "Is your printer \(selected)?", preferredStyle: .alert)
                
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            
            if self.isMinigame {
                
                self.searchViewDelegate?.printerInformation(model: selected,
                                                            port: self.printerPort[index],
                                                            macAddress: self.printerMac[index],
                                                            index: self.printerIndex ?? 0)
                
                self.alertConnectionSuccess() {
                    self.navigationController?.popViewController(animated: true)
                }
                
            } else {
                guard let printer = self.currentPrinter else {
                    return
                }
                                
                // show something is loading - for UX purposes
                // remove spinner once it has been selected or cannot connect, must be ran in the main thread
                DispatchQueue.main.async {
                    self.showActivitySpinner()
                }
                
                // check if currently selected printer is the same for connection,
                // if current printer is the same as modelName, alert successful and go back
                // otherwise input a new connection
                if selected == printer {
                    self.removeSpinner()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.searchViewDelegate?.printerInformation(model: selected,
                                                                    port: self.printerPort[index],
                                                                    macAddress: self.printerMac[index],
                                                                    index: self.printerIndex ?? 0)
                        
                        self.alertConnectionSuccess() {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                } else {
                    
                    // this check happens if interface has not been added or incorrect port name / port info for
                    // printer that is being used
                    DispatchQueue.main.async {
                        self.removeSpinner()
                    }
                    self.alertIncorrectDevice()
                }
            }
        }) // end ok button
    
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in

        })
        
        confirm.addAction(ok)
        confirm.addAction(cancel)
        
        self.present(confirm, animated: true, completion: nil)
    }
    
    func didSelectRefreshPortInterface(interface: String,
                                       completion: @escaping([String], [String], [String]) -> Void) {
        
        do {
            searchResult = try SMPort.searchPrinter(target: interface) as? [PortInfo]
        }
        catch {
            showManualInterface()
        }
        
        guard let portInfoArray: [PortInfo] = searchResult else {
            return
        }
        
        for portInfo: PortInfo in portInfoArray {
            printerPort.append(portInfo.portName)
            printerMac.append(portInfo.macAddress)
            printerModel.append(portInfo.modelName)
        }
        
        completion(printerModel, printerPort, printerMac)
                            
    }
    
    @IBAction func retryDiscovery(_ sender: Any) {
        resetPortInfo()
        showDiscoverySpinner()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.didSelectRefreshPortInterface(interface: "All:") { modelName, portName, portMac in
                self.removeSpinner()
                self.searchedPrintersTableView.reloadData()
                
                if self.printerModel.count == 0 {
                    self.alertNoDevicesFound()
                }
            }
        }
        
    }
}

extension SearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let printer = PrinterModel()
        let port_name = ModelPortName()
        
        // 'dummy' variable is used for testing printers not able to be tested since I do not have
        // the device available to test
        //let dummy  = "SM-S230"
        let row   = indexPath.row
        let model = printerModel[row]
        
        switch model {
        // LAN
        case port_name.mcprint20:
            showSelectedPrinter(selected: printer.mcp20, index: row)
        case port_name.mcprint21:
            showSelectedPrinter(selected: printer.mcp21, index: row)
        case port_name.mcprint30:
            showSelectedPrinter(selected: printer.mcp30, index: row)
        case port_name.mcprint31:
            showSelectedPrinter(selected: printer.mcp31, index: row)
        case port_name.mcLabel:
            showSelectedPrinter(selected: printer.mclabel, index: row)
        case port_name.tsp143iiiL:
            showSelectedPrinter(selected: printer.tsp100iii, index: row)
        case port_name.tsp143iiiW:
            showSelectedPrinter(selected: printer.tsp100iii, index: row)
        case port_name.tsp143iv:
            
            // how to differentiate the port between 143IV and 143IV SK if the port is the same? 
            if currentPrinter == "TSP143IV SK" {
                showSelectedPrinter(selected: printer.tsp100ivsk, index: row)
            } else {
                showSelectedPrinter(selected: printer.tsp100iv, index: row)
            }
            
        case port_name.tsp650ii:
            showSelectedPrinter(selected: printer.tsp650ii, index: row)
        case port_name.tsp700ii:
            showSelectedPrinter(selected: printer.tsp700ii, index: row)
        case port_name.tsp800ii:
            showSelectedPrinter(selected: printer.tsp800ii, index: row)
        case port_name.sp700:
            showSelectedPrinter(selected: printer.sp700, index: row)
            
        // Bluetooth
        case port_name.bt_mcprint21LB:
            showSelectedPrinter(selected: printer.mcp21, index: row)
        case port_name.bt_mcprint31LB:
            showSelectedPrinter(selected: printer.mcp31, index: row)
        case port_name.bt_mcprint31L:
            showSelectedPrinter(selected: printer.mcp31, index: row)
        case port_name.bt_mcprint31CBI:
            showSelectedPrinter(selected: printer.mcp31, index: row)
        case port_name.bt_mclabel3CI:
            showSelectedPrinter(selected: printer.mclabel, index: row)
        case port_name.bt_mclabel3CBI:
            showSelectedPrinter(selected: printer.mclabel, index: row)
        case port_name.bt_mpop:
            showSelectedPrinter(selected: printer.mpop, index: row)
        case port_name.bt_mpopci:
            showSelectedPrinter(selected: printer.mpop, index: row)
        case port_name.bt_mpopcbi:
            showSelectedPrinter(selected: printer.mpop, index: row)
        case port_name.bt_tsp143iiiUGY:
            showSelectedPrinter(selected: printer.tsp100iii, index: row)
        case port_name.bt_tsp143iiiUWHT:
            showSelectedPrinter(selected: printer.tsp100iii, index: row)
        case port_name.bt_tsp143iiiBiWT:
            showSelectedPrinter(selected: printer.tsp100iii, index: row)
        case port_name.bt_tsp143iiiBiGY:
            showSelectedPrinter(selected: printer.tsp100iii, index: row)
        case port_name.bt_tsp650ii:
            showSelectedPrinter(selected: printer.tsp650ii, index: row)
        case port_name.bt_tsp700ii:
            showSelectedPrinter(selected: printer.tsp700ii, index: row)
        case port_name.bt_tsp800ii:
            showSelectedPrinter(selected: printer.tsp800ii, index: row)
        case port_name.bt_sp700:
            showSelectedPrinter(selected: printer.sp700, index: row)
        case port_name.sm_s210i:
            showSelectedPrinter(selected: printer.sm_s210i, index: row)
        case port_name.sm_s230i:
            showSelectedPrinter(selected: printer.sm_s230i, index: row)
        case port_name.sm_l200:
            showSelectedPrinter(selected: printer.sm_l200, index: row)
        case port_name.sm_l300:
            showSelectedPrinter(selected: printer.sm_l300, index: row)
        case port_name.sm_t300:
            showSelectedPrinter(selected: printer.sm_t300, index: row)
        case port_name.sm_t400:
            showSelectedPrinter(selected: printer.sm_t400, index: row)
        default:
            alertUnknownInterface()
        }
    }
    
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return printerModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchPortTableViewCell.identifier, for: indexPath) as! SearchPortTableViewCell
        
        let row = indexPath.row
        
        cell.configureCell(model: printerModel[row],
                           portName: printerPort[row],
                           macAddress: printerMac[row])
        
        return cell
    }
}
