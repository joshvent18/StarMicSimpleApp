//
//  BarcodeScanViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 12/20/22.
//

import UIKit

class BarcodeScanViewController: UIViewController {
    
    //MARK: - IBOUTLET
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var parseLabel: UILabel!
    
    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var containerView: UIView!
                
    //MARK: - LIFECYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textview.delegate = self
        
        setupClearButton()
        setupContainer()
        setupTextview()
        
    }
        
    //MARK: - FUNCS, SETUP
    private func setupContainer() {
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor.black.cgColor
        containerView.layer.cornerRadius = 10
    }
    
    private func setupClearButton() {
        clearButton.layer.cornerRadius = 10
    }
    
    private func setupTextview() {
        textview.layer.cornerRadius = 10
        textview.layer.borderColor  = UIColor.systemGray4.cgColor
        textview.layer.borderWidth  = 1
    }
    
    //MARK: - IBACTION
    @IBAction func clearTextview(_ sender: Any) {

    }
    
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
}

//MARK: - EXTENSION TEXTVIEW DELEGATE
extension BarcodeScanViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text      = ""
        textview.textColor = UIColor.black
    }
}
