//
//  TradeShowProductSelectionViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/15/22.
//

import UIKit

class TradeShowProductSelectionViewController: UIViewController {
    
    @IBOutlet weak var chooseProductTypeLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var category: [String] = [ "Printers", "Cash Drawers", "Scales", "Stands", "Scanners"]
    var images: [String]   = [ "printer_linear_icon", "cashdrawer_linear_icon", "scale_linear_icon",
                               "stands_linear_icon", "scanner_linear_icon"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate   = self
        collectionView.register(ProductSelectionViewCell.nib(), forCellWithReuseIdentifier: ProductSelectionViewCell.identifier)
                
    }
    
    func transitionProductAdd(categoryID: Int) {
        let productAddSB = UIStoryboard.init(name: "TradeShowAdd", bundle: nil)
        let productAddVC = productAddSB.instantiateViewController(withIdentifier: "TSAdd") as! TradeShowAddProductViewController
        
        productAddVC.categoryID = categoryID
                                                    
        productAddVC.modalTransitionStyle   = .flipHorizontal
        productAddVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(productAddVC, animated: true)
    }
    
}

extension TradeShowProductSelectionViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        transitionProductAdd(categoryID: indexPath.row)
    }
    
}

extension TradeShowProductSelectionViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return category.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductSelectionViewCell.identifier, for: indexPath) as! ProductSelectionViewCell
        
        cell.configureCell(image: images[indexPath.row], type: category[indexPath.row])
        
        return cell
    }
    
}

extension TradeShowProductSelectionViewController: UICollectionViewDelegateFlowLayout {
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 20, left: 25, bottom: 20, right: 25)
    }
    
}
