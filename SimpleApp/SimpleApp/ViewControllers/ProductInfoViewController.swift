//
//  ProductInfoViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 10/3/22.
//

import UIKit

class ProductInfoViewController: UIViewController {
    
    @IBOutlet weak var nameLabel:        UILabel!
    @IBOutlet weak var typeLabel:        UILabel!
    @IBOutlet weak var taglineLabel:     UILabel!
    @IBOutlet weak var featuresLabel:    UILabel!
    @IBOutlet weak var descriptionText: UILabel!
    @IBOutlet weak var featuresText: UILabel!

    @IBOutlet weak var modelAndPartNumbersButton: UIButton!
    
    //MARK: - VIEWS
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var scrollViewContainer: UIView!
    @IBOutlet weak var picturesCollectionView: UICollectionView!
    
    var features:            Items?
    var modelAndPartNumbers: ModelInformation?
    var name:                String   = ""
    var type:                String   = ""
    var tagline:             String   = ""
    var productDescription:  String   = ""
    var imageName:           String?
    var images:              [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picturesCollectionView.register(ProductsImageCollectionViewCell.nib(),
                                        forCellWithReuseIdentifier: ProductsImageCollectionViewCell.identifier)
        
        picturesCollectionView.dataSource = self
        picturesCollectionView.delegate   = self
        
        configureView(productName: name, productType: type, productTagline: tagline,
                      productDescription: productDescription, productImages: imageName!)
                        
        parseJSON()
    }
    
    //MARK: - viewWillTransition
    /*
        Update the layout after changing the orientation
     */
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate { _ in
            self.picturesCollectionView.collectionViewLayout.invalidateLayout()
        }
    }
    
    @IBAction func showModelsAndPartNumbers(_ sender: Any) {
        
        let partNumbersSB = UIStoryboard.init(name: "PartNumbers", bundle: nil)
        let partNumbersVC = partNumbersSB.instantiateViewController(withIdentifier: "PartNumbers") as! PartNumbersViewController
        
        // set data for product numbers and model
        partNumbersVC.dataSource = modelAndPartNumbers
         
        // transition to part numbers
        partNumbersVC.modalPresentationStyle = .fullScreen
        partNumbersVC.modalTransitionStyle   = .coverVertical
        partNumbersVC.isModalInPresentation  = false
        self.present(partNumbersVC, animated: true)
        
    }
    
    /*
        Configure cells
        - Parameters:
            - productName:          Product name
            - productType:          The product type ("Thermal", "Scanner", etc...)
            - productTagline:       Tagline to describe product
            - productDescription:   Description of product
            - productImages:        Image of product
     */
    func configureView(productName: String, productType: String, productTagline: String, productDescription: String,
                       productImages: String) {
        
        nameLabel.text        = productName
        typeLabel.text        = productType
        taglineLabel.text     = productTagline
        descriptionText.text  = productDescription
        images.append(productImages)
        
        if( (productType.lowercased() == PrinterType.impact.rawValue) ||
            (productType.lowercased() == PrinterType.kiosk.rawValue) ||
            (productType.lowercased() == PrinterType.portable.rawValue) ||
             (productType.lowercased() == PrinterType.thermal.rawValue) )
        {
            typeLabel.text = type + " Receipt Printer"
        } else {
            typeLabel.text = type
        }
        
    }

    func parseJSON() {
        guard let path = Bundle.main.path(forResource: "features", ofType: "json") else {
            return
        }

        let url = URL(fileURLWithPath: path)

        do {
            var split: String = ""

            let data = try Data(contentsOf: url)
            features = try JSONDecoder().decode(Items.self, from: data)

            for models in features?.model ?? [] {
                if name == models.name {
                    split = models.features.joined(separator: "\n")
                    featuresText.text = split
                }
            }

            // add line spacing in textview
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 10

            let attributes = [NSAttributedString.Key.paragraphStyle: style,
                              NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: descriptionText.font.pointSize),
                              NSAttributedString.Key.foregroundColor: UIColor.white]
            featuresText.attributedText = NSAttributedString(string: split, attributes: attributes)

        } catch {
            print("Error: \(error)")
        }

    }
    
}

extension ProductInfoViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
}

extension ProductInfoViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductsImageCollectionViewCell.identifier, for: indexPath) as! ProductsImageCollectionViewCell
        
        cell.configure(image: images[indexPath.row])
        
        return cell
    }
}
