//
//  SoundManagerViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 12/15/22.
//

import AVKit
import UIKit

class SoundManager {
    
    static let instance = SoundManager()
    
    var player: AVAudioPlayer?
    
    func play(resource: String, fileFormat: String) {
        guard let pathUrl = Bundle.main.url(forResource: "tada", withExtension: ".mp3") else {
            return
        }
        
        do {
            player = try AVAudioPlayer.init(contentsOf: pathUrl)
            player?.play()
        } catch let error {
            print("Error playing sound.\(error.localizedDescription)")
        }
    }
}
