//
//  ScannerViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 3/24/24.
//

import UIKit
import StarSCAN
import CoreBluetooth

class ScannerViewController: UIViewController {

    var birthdate: String?
    var firstName: String?
    var lastName: String?
    var driversLicenseVerification: Bool?
    var isDriversLicense: Bool?
    
    var timer: Timer              = Timer()
    var stringArray: [String]     = []
    var valid_age_seconds: Double = 662688000   // 21 years in seconds
    var year: Double              = 31536000    // seconds in a year
    let format: String            = "MM/dd/yyyy"
    
    let color_green: UIColor = UIColor(red: 0.00, green: 0.55, blue: 0.01, alpha: 1.00)
    let color_red: UIColor   = UIColor(red: 1.00, green: 0.00, blue: 0.00, alpha: 1.00)
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var scannerLabel: UILabel!
    @IBOutlet weak var licenseInfoLabel: UILabel!
    
    @IBOutlet weak var switchButton: UISwitch!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var backgroundView: UIView!
    
    var remote: StarSCANPeripheral!
    var availablePeripherals: [StarSCANPeripheral] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        StarSCANCentral.instance.centralDelegate = self
                
        // init UI
        switchButton.isOn          = false
        driversLicenseVerification = false
        licenseInfoLabel.isHidden  = true
        setupTextView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        StarSCANCentral.instance.startScanAndConnect()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // get current connected and put it in the list of connected
        availablePeripherals = StarSCANCentral.instance.getControllablePeripherals()
        
        if availablePeripherals.isEmpty {
            setupBlinkingText(isConnected: false)
            statusLabel.textColor = .systemRed
                
            alertScannersNotFound()
        } else {
                
            remote = availablePeripherals.first
            remote.peripheralDelegate = self
                                
            if remote.isReady {
                setupBlinkingText(isConnected: true)
            } else {
                setupBlinkingText(isConnected: false)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    @IBAction func switchPressed(_ sender: Any) {
        if switchButton.isOn {
            driversLicenseVerification = true
            textView.text.removeAll()
        } else {
            driversLicenseVerification = false
            textView.text.removeAll()
        }
    }
    
    func alertScannersFound(scannerName: String) {
        let confirm = UIAlertController(title: "Scanner found",
                                        message: "Connected to \(scannerName) and is ready to scan",
                                        preferredStyle: .alert)

        let ok = UIAlertAction(title: "Lets scan!", style: .default, handler: { (action) -> Void in
            // dismiss and do nothing
        })
        
        confirm.addAction(ok)
     
        self.present(confirm, animated: true, completion: nil)
    }
    
    func alertScannerIsNotReady() {
        let confirm = UIAlertController(title: "Failed to connect scanner",
                                        message: "Make sure the scanner is powered on and paired with this device, then click Connect New Scanner to retry.",
                                        preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            // dismiss and do nothing
        })
        
        confirm.addAction(ok)
     
        self.present(confirm, animated: true, completion: nil)
    }
        
    func alertScannersNotFound() {
        let confirm = UIAlertController(title: "No scanners found. Would you like to connect a scanner?",
                                        message: "",
                                        preferredStyle: .alert)

        let ok = UIAlertAction(title: "Connect", style: .default, handler: { (action) -> Void in
            self.connectScanner()
        })
        
        let connect = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in
            
        })

        confirm.addAction(ok)
        confirm.addAction(connect)

        self.present(confirm, animated: true, completion: nil)
    }
    
    /*
        Performed only when Connect New Scanner button is pressed
     */
    @IBAction func connectButtonPressed(_ sender: Any) {
        // connect scanner after the first time
        let name: String? = nil // "BS50 BG00098"
        let identifier: UUID? = nil // UUID(uuidString: "024CA0D0-8F4D-FB87-3F52-940D47215C77")
 
        let ret = StarSCANCentral.instance.useSystemConnectedPeripheral(name, identifier)
        
        if ret {
            setupBlinkingText(isConnected: true)
        }
    }
    
    /*
        Performed when scanner is connecting from the alert
     */
    func connectScanner() {
        // connect scanner after the first time
        let name: String? = nil // "BS50 BG00098"
        let identifier: UUID? = nil // UUID(uuidString: "024CA0D0-8F4D-FB87-3F52-940D47215C77")
 
        let ret = StarSCANCentral.instance.useSystemConnectedPeripheral(name, identifier)
        
        if !ret {
            alertScannerIsNotReady()
        }
    }
    
    @IBAction func disconnectButtonPressed(_ sender: Any) {
        print("Disconnecting scanner....")
        let ret = StarSCANCentral.instance.clearDisconnectedPeripherals()
        print("Disconnect Scanner, ret count: \(ret.count)")
    }
    
    func getDateDifference(date: String) -> Double {
        // get todays seconds since 1970
        let today                = getCurrentDateString()
        let currentTimeInSeconds = today.dateInSeconds(dateFormat: format)
            
        // get birth date in seconds
        let timeSinceDOB = date.dateInSeconds(dateFormat: format)
            
        // calculate time since birthday in seconds
        let timeDifference = currentTimeInSeconds - timeSinceDOB
            
        return timeDifference
    }
    
    func textViewBlinkRed() {
        updateTextViewBorderColor(borderCol: color_red.cgColor)
        backgroundView.isHidden = false
        backgroundView.backgroundColor = color_red
        backgroundView.blink()
    }
    
    func textViewBlinkGreen() {
        updateTextViewBorderColor(borderCol: color_green.cgColor)
        backgroundView.isHidden = false
        backgroundView.backgroundColor = color_green
        backgroundView.blink()
    }
    
    func setupTextView() {
        textView.text = "Always Leading, Always Innovating!"
        textView.font = .systemFont(ofSize: 22.0)
        textView.layer.cornerRadius = 5
        textView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        textView.layer.borderWidth = 1.0
        textView.clipsToBounds = true
        
        backgroundView.isHidden = true
        backgroundView.layer.cornerRadius = 10.0
    }
    
    func resetTextView() {
        self.updateTextViewBorderColor(borderCol: UIColor.gray.withAlphaComponent(0.5).cgColor)
        self.textView.layer.borderWidth = 1.0
        self.backgroundView.isHidden = true
    }
    
    func resetLicenseInfo() {
        licenseInfoLabel.isHidden = true
        licenseInfoLabel.text     = ""
    }
    
    func updateTextViewBorderColor(borderCol: CGColor) {
        textView.layer.borderColor = borderCol
    }
    
    func setupBlinkingText(isConnected: Bool) {
        if isConnected {
            timer.invalidate()
            statusLabel.text      = "Barcode scanner connected."
            statusLabel.textColor = .systemBlue
        } else {
            timer.fire()
            self.timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { _ in
                self.statusLabel.blink()
            }
            statusLabel.text      = "Barcode scanner disconnected. Check the device."
            statusLabel.textColor = .systemRed
        }
    }
        
    func clearTextView() {
        textView.text = ""
    }
    
    @objc func triggerCheckData() {
        checkData()
    }
    
    func calculateAge(ageSeconds: Double) -> Double {
        let age = ageSeconds / year
        return age.rounded(.towardZero)
    }
    
    func checkData() {
        
        let data = stringArray.joined()
        
        isDriversLicense = false
                
        if driversLicenseVerification! {
            parseScannedData(scannedData: data)
            
            if isDriversLicense! {
                
                guard let licenseDate = birthdate else {
                    return
                }
                
                let age_seconds = getDateDifference(date: licenseDate)
                let current_age = calculateAge(ageSeconds: age_seconds)

                if current_age < 21 {
                    let current_age_string = String(format: "%.0f", current_age)
                    
                    textViewBlinkRed()
                    
                    licenseInfoLabel.isHidden = false
                    licenseInfoLabel.text = "\(firstName!) " + "\(lastName!) " + "   Age: \(current_age_string)"
                    licenseInfoLabel.textColor = color_red
                }
                                
                if current_age >= 21 {
                    let current_age_string = String(format: "%.0f", current_age)
                    
                    textViewBlinkGreen()
                    
                    licenseInfoLabel.isHidden = false
                    licenseInfoLabel.text = "\(firstName!) " + "\(lastName!) " + "   Age: \(current_age_string)"
                    licenseInfoLabel.textColor = color_green
                }
                                        
                DispatchQueue.main.asyncAfter(deadline: .now() + 7.0) {
                    self.resetTextView()
                }
            } else {
                alertInvalidLicense()
            }
                        
        } else {
            clearTextView()
            
            textView.text = data
            
            resetTextView()
            resetLicenseInfo()
        }
        
        // reset array
        stringArray.removeAll()
        
        // stop timer
        timer.invalidate()
    }
    
    func parseScannedData(scannedData: String) {
        let regex_dob         = "(DBB)"
        let regex_firstName   = "(DAC)"
        let regex_lastName    = "(DCS)"
        let regex_ansi        = "(ANSI)"
        
        // use ANSI header to determine if barcode is drivers license other
        if let range = scannedData.range(of: regex_ansi, options: .regularExpression) {
            isDriversLicense = true
        }
    
        if let range = scannedData.range(of: regex_firstName, options: .regularExpression) {
            
            var givenNameArray: [Character] = []
            let givenName = String(scannedData[range.upperBound...])
            
            // iterate through the characters and store them in the array. Break the loop when it finds the \n character
            for char in givenName {
                givenNameArray.append(char)
                if char.isNewline {             // stop iterating
                    break
                }
            }
            
            // remove the last character since it's a new line anyways
            givenNameArray.removeLast()
            firstName    = String(givenNameArray)
            
            print("Given Name: ", String(givenNameArray))

        }
        
        if let range = scannedData.range(of: regex_lastName, options: .regularExpression) {
            var maidenName: [Character] = []
            let maiden = String(scannedData[range.upperBound...])
            
            for char in maiden {
                maidenName.append(char)
                if char.isNewline {
                    break
                }
            }
            
            maidenName.removeLast()
            lastName = String(maidenName)
            
            print("Maden Name: ", String(maidenName))

        }
        
        if let range = scannedData.range(of: regex_dob, options: .regularExpression) {
            
            var dob = String(scannedData[range.upperBound...].prefix(8))
            
            // insert '/' at the proper indexes for checking DateFormatter()
            dob.insert("/", at: dob.index(dob.startIndex, offsetBy: 2))
            dob.insert("/", at: dob.index(dob.endIndex, offsetBy: -4))

            birthdate = dob
            print("DOB:", dob)

        }
    }
    
}

extension ScannerViewController: StarSCANCentralDelegate, StarSCANPeripheralDelegate  {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("Did update state \(central.state)")
        switch central.state {
        case .unknown:
            break
        case .unsupported:
            print("Did update state unsupported")
        case .unauthorized:
            print("Did update state unauthorized")
        case .poweredOff:
            print("Did update state powered off")
        case .poweredOn:
            print("Did update state powered on")
        case .resetting:
            print("Did update state reseting")
        default:
            break
        }
    }
    
    func onStartConnecting(_ peripheral: CBPeripheral, _ reconnect: Bool) {
        print("Scanner did connect")
        timer.fire()
        setupBlinkingText(isConnected: true)
    }
    
    func onStopConnecting(_ peripheral: CBPeripheral) {
        StarSCANCentral.instance.stopScan()
        print("Scanner stopped connecting")
        setupBlinkingText(isConnected: true)
    }
    
    func onPeripheralReady(_ peripheral: StarSCANPeripheral) {
        print("ready to scann")
        availablePeripherals = StarSCANCentral.instance.getControllablePeripherals()
        remote = availablePeripherals.first
        remote.peripheralDelegate = self
        
        alertScannersFound(scannerName: remote.name!)
    }
    
    func onDidDisconnect(_ peripheral: StarSCANPeripheral) {
        print("Peripheral did disconnect")
    }
    
    func onScanDataReceived(_ peripheral: StarSCANPeripheral, _ data: Data) {
        let str = String(data: data, encoding: .utf8) ?? ""
        stringArray.append(str)
        
        // timer for trigger
        if !timer.isValid {
            timer = Timer.scheduledTimer(timeInterval: 1,
                                         target: self,
                                         selector: #selector(triggerCheckData),
                                         userInfo: nil,
                                         repeats: false)
        }
    }
    
}
