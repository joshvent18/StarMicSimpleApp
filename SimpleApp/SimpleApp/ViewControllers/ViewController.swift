//
//  ViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 2/2/22.
//

import UIKit
import DropDown

/*
    Class for the landing page. This will show the main product page for the app
 */

class ViewController: UIViewController  {
        
    @IBOutlet weak var hamburgerMenu: UIBarButtonItem!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var printersLabel: UILabel!
    
    // MARK: - COLLECTION VIEW CELLS
    @IBOutlet var collectionView: UICollectionView!
    
    @IBOutlet weak var dropDownMenuView: UIView!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var dropDownLabel: UILabel!
    @IBOutlet weak var dropDownImage: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    
    // MARK: - CONSTRAINTS
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstrait: NSLayoutConstraint!
    
    // MARK: - DATA SOURCE
    var products: Category?
    var productList: [Model]?
    var menuTapped: Bool = false
    
    let selection = ["Printers", "Cashdrawers", "Scales", "Stands", "Scanners"]
    
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // layout
        self.navigationItem.hidesBackButton = true
        view.backgroundColor = UIColor(named: "star-blue")
        collectionView.backgroundColor = UIColor(named: "star-blue")
                
        // register nib
        collectionView.register(ProductsCollectionViewCell.nib(), forCellWithReuseIdentifier: ProductsCollectionViewCell.identifier)
        
        // delegates
        collectionView.dataSource = self
        collectionView.delegate   = self
        
        // keep label white on dark mode
        if self.traitCollection.userInterfaceStyle == .dark {
            printersLabel.textColor = UIColor.white
        } else {
            printersLabel.textColor = UIColor.white
        }

        setupDropDown()
        parseJSON()
    }
    
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate { _ in
            self.collectionView.collectionViewLayout.invalidateLayout()
        }
    }
    
    @IBAction func showDropDown(_ sender: Any) {
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            dropDownLabel.text = item
            parseJSON()
            collectionView.reloadData()
            
            changeCategoryLabelText(category: item.lowercased())
        }
    }
    
    func changeCategoryLabelText(category: String) {
        switch category {
        case ProductCategory.cashdrawers.rawValue:
            categoryLabel.text = "Cash Drawers"
        case ProductCategory.printers.rawValue:
            categoryLabel.text = "Receipt Printers"
        case ProductCategory.scales.rawValue:
            categoryLabel.text = "POS Scales"
        case ProductCategory.stands.rawValue:
            categoryLabel.text = "mUnite Stands"
        case ProductCategory.scanners.rawValue:
            categoryLabel.text = "Scanners"
        default:
            categoryLabel.text = "Products"
        }
    }

    func setupDropDown() {
        dropDown.dataSource = selection
        dropDown.anchorView = dropDownMenuView
        dropDownLabel.text  = selection[0]
        dropDownButton.setTitle("", for: .normal)
        dropDownButton.titleLabel?.isHidden = true
        dropDown.direction = .bottom
        
        // change label according to product
        changeCategoryLabelText(category: selection[0].lowercased())
    }
    
    func testNetworkManager() {
        
        // try a test call to the api
        let networkManager = NetworkManager()
        guard let url = URL(string: "https://starmicronics.com/wp-json/wc/v3/products/attributes") else {
            fatalError("Invalid URL")
        }
        
        let userName = "ck_8be77a9899978fe14834d6d955e9b3e2d87fee8b"
        let password = "cs_029551e0c4c96cbcf923ee1ab76f8b46efd5465f"
        
//        networkManager.requestWithBasicAuth(fromURL: url, withUserName: userName, withPassword: password) { (result: Result<[Answer], Error>) in
//            switch result {
//            case .success(let answer):
//                debugPrint("We got a succesful result with \(answer)")
//            case .failure(let error):
//                debugPrint("We got a failure trying to get the users. The error we got was: \(error)")
//            }
//        }
    }
            
    func parseJSON() {

        guard let path = Bundle.main.path(forResource: "category", ofType: "json") else {
            return
        }
        
        let url = URL(fileURLWithPath: path)
        
        do {
            let data = try Data(contentsOf: url)
            products = try JSONDecoder().decode(Category.self, from: data)
        } catch {
            print("Error: \(error)")
        }
        
        guard let category = dropDownLabel.text else {
            return
        }
        
        switch category.lowercased() {
            case ProductCategory.printers.rawValue:
                productList = products?.printers
            case ProductCategory.cashdrawers.rawValue:
                productList = products?.cashdrawers
            case ProductCategory.scales.rawValue:
                productList = products?.scales
            case ProductCategory.stands.rawValue:
                productList = products?.stands
            case ProductCategory.scanners.rawValue:
                productList = products?.scanners
            default:
            print("failed")
        }
    }

}

extension ViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let result = productList {
            
            let infoSB = UIStoryboard.init(name: "ProductInfo", bundle: nil)
            let infoVC = infoSB.instantiateViewController(withIdentifier: "ProductInfoViewController") as! ProductInfoViewController
                                         
            infoVC.name                = result[indexPath.row].item
            infoVC.type                = result[indexPath.row].type
            infoVC.tagline             = result[indexPath.row].tagline
            infoVC.productDescription  = result[indexPath.row].description
            infoVC.modelAndPartNumbers = result[indexPath.row].model
            infoVC.imageName           = result[indexPath.row].image
                        
            infoVC.modalTransitionStyle   = .flipHorizontal
            infoVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.pushViewController(infoVC, animated: true)
            
        } else {
            print("Error")
        }
    }
    
}

extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if let result = productList {
            return result.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductsCollectionViewCell.identifier, for: indexPath) as! ProductsCollectionViewCell
        
        // fill information
        if let result = productList {
            cell.configure(name: result[indexPath.row].item,
                           type: result[indexPath.row].type,
                           imageName: result[indexPath.row].image)
        }
            
        return cell
    }
    
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = view.frame.size.width
        let height = view.frame.size.height
        
        // layout for device orientation
        if UIDevice.current.orientation.isFlat {
            if width > height {
                let spacing: CGFloat = 5
                let totalHorizontalSpacing = (3 - 1) * spacing

                let itemWidth = (collectionView.bounds.width - totalHorizontalSpacing) / 3
                let itemSize = CGSize(width: itemWidth, height: itemWidth * 1.2)

                return itemSize
            } else {
                let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
                let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
                let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
                
                return CGSize(width: size, height: size)
            }
        } else {
            
            // device is in LANDSCAPE (width > height) || device is in PORTRAIT (height > width)
            if width > height {
                let spacing: CGFloat = 5
                let totalHorizontalSpacing = (3 - 1) * spacing

                let itemWidth = (collectionView.bounds.width - totalHorizontalSpacing) / 3
                let itemSize = CGSize(width: itemWidth, height: itemWidth * 1.2)

                return itemSize
            } else {
                let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
                let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
                let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
                
                return CGSize(width: size, height: size)
            }
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    }
    
}
