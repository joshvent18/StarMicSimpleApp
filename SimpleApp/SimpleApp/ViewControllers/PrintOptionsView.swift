//
//  PrintOptionsView.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 7/27/22.
//

import UIKit

class PrintOptionsView: UIViewController {
    
    // MARK: - PROPERTIES
    
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var subheader: UILabel!
    
    @IBOutlet weak var printOptions: UITableView!
    
    private var options = ["Text", "Image"]
    
    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        printOptions.delegate   = self
        printOptions.dataSource = self
        
    }
    
    // MARK: - FUNCTIONS
    func printText() {
        var command: [UInt8] = [0x41, 0x42, 0x43, 0x44, 0x1b, 0x7a, 0x00, 0x1b, 0x64, 0x02]

        while true {
            var port : SMPort

            do {
            // Open port
            port = try SMPort.getPort(portName: "TCP:10.0.0.142", portSettings: "", ioTimeoutMillis: 10000)

            defer {
                // Close port
                SMPort.release(port)
            }

            var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()

            // Start to check the completion of printing
            try port.beginCheckedBlock(starPrinterStatus: &printerStatus, level: 2)

            if printerStatus.offline == sm_true {
                break    // The printer is offline.
            }

            var total: UInt32 = 0

            while total < UInt32(command.count) {
                var written: UInt32 = 0

                // Send print data
                try port.write(writeBuffer: command, offset: total, size: UInt32(command.count) - total, numberOfBytesWritten: &written)

                total += written
            }

            // Stop to check the completion of printing
            try port.endCheckedBlock(starPrinterStatus: &printerStatus, level: 2)

            if printerStatus.offline == sm_true {
                break    // The printer is offline.
            }

            // Success
            break
            }
            catch let error as NSError {
            break    // Some error occurred.
            }
        }
    }
        
}

extension PrintOptionsView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
}

extension PrintOptionsView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "print", for: indexPath)
        
        cell.textLabel?.text = options[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = indexPath.row
        
        switch selected {
        case 0:
            printText()
            break;
        case 1:
            print("image selected")
            break;
        default:
            break;
        }
    }
    
}
