//
//  ScaleOutputViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 1/4/23.
//

import UIKit

/*
    Class for Scale output.
    This is the view that will show the output of the scale.
 */
class ScaleOutputViewController: UIViewController {
    
    var scale: STARScale?

    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dataTypeLabel: UILabel!
    @IBOutlet weak var comparatorLabel: UILabel!
    
    // MARK: - LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        STARDeviceManager.shared().disconnectScale(scale!)
    }
    
    // MARK: - FUNCTIONS
    func getUnit(unit: STARUnit) -> String {
        var someUnit = ""
        
        switch unit {
        case .invalid:  someUnit = "Invalid"
        case .MG:       someUnit = "mg"
        case .G:        someUnit = "g"
        case .KG:       someUnit = "kg"
        case .CT:       someUnit = "lb"
        case .MOM:      someUnit = "mom"
        case .OZ:       someUnit = "oz"
        case .LB:       someUnit = "lb"
        case .OZT:      someUnit = "ozt"
        case .DWT:      someUnit = "dwt"
        case .GN:       someUnit = "gr"
        case .TLH:      someUnit = "tl"
        case .TLS:      someUnit = "tl"
        case .TLT:      someUnit = "tl"
        case .TO:       someUnit = "tola"
        case .MSG:      someUnit = "msg"
        case .BAT:      someUnit = "bht"
        case .PCS:      someUnit = "PCS"
        case .percent:  someUnit = "%"
        case .coefficient: someUnit = "MUL"
        default:
            break
        }
        
        return someUnit
    }
    
    func getStatus(status: STARStatus) -> String {
        var currentStatus = ""
        
        switch status {
        case .stable:   currentStatus = "Stable"
        case .unstable: currentStatus = "Unstable"
        case .error:    currentStatus = "Error"
        case .invalid:  currentStatus = "Invalid"
        default:
            break
        }
        
        return currentStatus
    }
    
    func getDataType(dataType: STARDataType) -> String {
        var type = ""
        
        switch dataType {
        case .invalid:      type = "Invalid"
        case .netNotTared:  type = "NetNotTared"
        case .net:          type = "Net"
        case .tare:         type = "Tare"
        case .presetTare:   type = "PresetTare"
        case .total:        type = "Total"
        case .unit:         type = "Unit"
        case .gross:        type = "Gross"
        default:
            break
        }
        
        return type
    }
    
    func getComparatorResult(comparator: STARComparatorResult) -> String {
        var result = ""
        
        switch comparator {
        case .invalid:  result = "Invalid"
        case .over:     result = "Over"
        case .proper:   result = "Proper"
        case .shortage: result = "Shortage"
        default:
            break
        }
        
        return result
    }
        
}

// MARK: - STARScale DELEGATE
extension ScaleOutputViewController: STARScaleDelegate {
    
    func scale(_ scale: STARScale!, didRead scaleData: STARScaleData!, error: Error!) {
        let decimalPlace: Int = scaleData.numberOfDecimalPlaces
        weightLabel.text     = String(format: "%.\(decimalPlace)f", scaleData.weight) +
                                " \(getUnit(unit: scaleData.unit))"
        statusLabel.text     = "Status: \(getStatus(status: scaleData.status))"
        dataTypeLabel.text   = "Data type: \(getDataType(dataType: scaleData.dataType))"
        comparatorLabel.text = "Comparation result: \(getComparatorResult(comparator: scaleData.comparatorResult))"
        
        if (scale.scaleType == STARScaleType_ENUM.MGTS) {
            comparatorLabel.isHidden = true
        } else {
            comparatorLabel.isHidden = false
        }
    }
    
    func scale(_ scale: STARScale!, didUpdate setting: STARScaleSetting, error: Error!) {
        // Not implemented yet
        print("updating")
    }
    
}
