//
//  PartNumbersViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 10/24/22.
//

import UIKit

class PartNumbersViewController: UIViewController {

    // MARK: - LABELS
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - DATA SOURCE
//    var dataSource: ProductModel?
    var dataSource: ModelInformation?
            
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(PartNumbersTableViewCell.nib(),
                           forCellReuseIdentifier: PartNumbersTableViewCell.identifier)
        
        tableView.delegate   = self
        tableView.dataSource = self
    
        tableView.separatorColor = .clear
        closeButton.setTitle("", for: .normal)
    }
    
    @IBAction func dismissPage(_ sender: Any) {
        self.dismiss(animated: true)
    }
        
}

extension PartNumbersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PartNumbersTableViewCell.identifier, for: indexPath) as! PartNumbersTableViewCell

        cell.selectionStyle = .none

        var modelName: [String]
        var modelNumber: [String]
        var interface: [String]
    
        if let printer = dataSource {

            modelName   = printer.modelName
            modelNumber = printer.modelNumber
            interface   = printer.modelDescription

            cell.configure(modelName: modelName[indexPath.row],
                           partNo: modelNumber[indexPath.row],
                           details: interface[indexPath.row])

        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let models = dataSource {
            return models.modelName.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row % 2) == 0 {
            cell.backgroundColor = UIColor.white
        } else {
            cell.backgroundColor = UIColor.systemGray6
        }
    }
}
