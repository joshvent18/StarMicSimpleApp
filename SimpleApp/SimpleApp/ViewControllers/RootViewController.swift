//
//  RootViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 10/3/22.
//

import UIKit

class RootViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setupNavigationBar() {
        self.title = "Catalog"

        let exitButton = UIBarButtonItem(title: "Exit", style: .plain, target: self, action: #selector(exitCatalog))
        navigationItem.leftBarButtonItem = exitButton
        
        self.navigationController?.navigationBar.topItem?.backButtonTitle = "Exit"
        self.navigationController?.navigationBar.backgroundColor          = .white
        self.navigationController?.navigationItem.hidesBackButton         = true
        self.navigationController?.navigationItem.leftBarButtonItem       = exitButton
    }
        
    // Action for exit button
    @objc func exitCatalog() {
        navigationController?.popViewController(animated: true)
    }
}
