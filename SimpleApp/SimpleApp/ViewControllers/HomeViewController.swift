//
//  HomeViewController.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 7/19/23.
//

import UIKit

class HomeViewController: UIViewController {
    
    let buttonRadius = 15.0
    
    @IBOutlet weak var welcomeToStarLabel: UILabel!
    @IBOutlet weak var selectOptionLabel: UILabel!
    @IBOutlet weak var versionNumberLabel: UILabel!
    @IBOutlet weak var starIOVersionLabel: UILabel!
    
    @IBOutlet weak var catalogButton: UIButton!
    @IBOutlet weak var tradeshowButton: UIButton!
    
    @IBOutlet weak var starLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        welcomeToStarLabel.alpha = 0.0
        selectOptionLabel.alpha = 0.0
        
        view.sendSubviewToBack(starLogo)
        setupButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
        
    override func viewDidAppear(_ animated: Bool) {
        
        guard let appVersion = Bundle.main.releaseVersionNumber else {
            return
        }
        
        guard let buildNumber = Bundle.main.buildVersionNumber else {
            return
        }
                
        versionNumberLabel.text = "Version: \(appVersion).\(buildNumber)"
        starIOVersionLabel.text = "StarIO Library Version \(getStarIOVersionNumber())"
        
        versionNumberLabel.fadeIn(duration: 0.2, delay: 0)
        self.selectOptionLabel.fadeIn(duration: 0.2, delay: 0)
        self.welcomeToStarLabel.fadeIn(duration: 0.2, delay: 0) { done in
            self.animateButtons(duration: 1.0, delay: 0.2, scaleX: 1.0, scaleY: 1.0)
        }
        
    }
    
    func setupButtons() {
        catalogButton.setRadiusWithShadow(buttonRadius)
        catalogButton.setTitle("", for: .normal)
        catalogButton.transform = CGAffineTransformMakeScale(0.1, 0.1)
        catalogButton.alpha     = 0
        
        tradeshowButton.setTitle("", for: .normal)
        tradeshowButton.setRadiusWithShadow(buttonRadius)
        tradeshowButton.transform = CGAffineTransformMakeScale(0.1, 0.1)
        tradeshowButton.alpha     = 0
    }
    
    func animateButtons(duration: TimeInterval, delay: TimeInterval, scaleX: CGFloat, scaleY: CGFloat) {
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.catalogButton.transform   = CGAffineTransformMakeScale(scaleX, scaleY)
            self.tradeshowButton.transform = CGAffineTransformMakeScale(scaleX, scaleY)
            self.catalogButton.alpha   = 1.0
            self.tradeshowButton.alpha = 1.0
        })
    }
    
    @IBAction func presentCatalog(_ sender: Any) {
        let catalogSB = UIStoryboard(name: "Main", bundle: nil)
        let catalogVC = catalogSB.instantiateViewController(withIdentifier: "MainViewController") as! RootViewController
        
        catalogVC.modalTransitionStyle   = .flipHorizontal
        catalogVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(catalogVC, animated: true)
    }
    
    @IBAction func presentTradeshow(_ sender: Any) {
        let tradeshowSB = UIStoryboard(name: "TradeShowMain", bundle: nil)
        let tradeshowVC = tradeshowSB.instantiateViewController(withIdentifier: "TradeShowMain") as! TradeShowMainViewController
        
        tradeshowVC.modalTransitionStyle   = .flipHorizontal
        tradeshowVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(tradeshowVC, animated: true)
    }
    
}

extension UIView {
    func setRadiusWithShadow(_ radius: CGFloat? = nil) { // this method adds shadow to right and bottom side of button
        self.layer.cornerRadius = radius ?? self.frame.width / 2
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        self.layer.shadowRadius = 1.0
        self.layer.shadowOpacity = 0.7
        self.layer.masksToBounds = false
    }

    func setAllSideShadow(shadowShowSize: CGFloat = 1.0) { // this method adds shadow to allsides
        let shadowSize : CGFloat = shadowShowSize
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: self.frame.size.width + shadowSize,
                                                   height: self.frame.size.height + shadowSize))
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.8).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    func fadeIn(duration: TimeInterval, delay: TimeInterval, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
            }, completion: completion)  }

    func fadeOut(duration: TimeInterval = 1.0, delay: TimeInterval = 3.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
            }, completion: completion)
    }
}
