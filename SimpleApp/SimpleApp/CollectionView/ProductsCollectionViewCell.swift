//
//  ProductsCollectionViewCell.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 9/12/22.
//

import UIKit

class ProductsCollectionViewCell: UICollectionViewCell {
        
    // MARK: - VARIABLES
    var cornerRadius = 10.0
    
    // MARK: - OUTLETS
    @IBOutlet var productLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    // MARK: - CELL IDENTIFIER
    static let identifier = "ProductsCollectionViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        contentView.layer.cornerRadius  = cornerRadius
        contentView.layer.masksToBounds = true
        
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = false
    }
    
    public func configure(name: String, type: String, imageName: String) {
        
        // make color of text black in dark mode or light mode
        if self.traitCollection.userInterfaceStyle == .dark {
            productLabel.textColor = UIColor.black
        } else {
            productLabel.textColor = UIColor.black
        }
        
        if let productImage = UIImage(named: imageName) {
            image.image = productImage
        } else {
            image.image = UIImage(named: "image-not-available")
        }
        
        // check product type
        if( (type.lowercased() == PrinterType.impact.rawValue) || (type.lowercased() == PrinterType.kiosk.rawValue) ||
            (type.lowercased() == PrinterType.portable.rawValue) || (type.lowercased() == PrinterType.thermal.rawValue) )
        {
            typeLabel.text = type + " Receipt Printer"
        } else {
            typeLabel.text = type
        }
                
        productLabel.text = name
        
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "ProductsCollectionViewCell", bundle: nil)
    }

}

extension UICollectionViewCell {
    
    // set corner radius 
    func cellCornerRadius(view: UIView, radius: CGFloat) {
        view.layer.cornerRadius = radius
    }
    
}
