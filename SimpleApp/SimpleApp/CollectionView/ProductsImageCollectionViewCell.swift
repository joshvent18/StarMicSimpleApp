//
//  ProductsImageCollectionViewCell.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 10/28/22.
//

import UIKit

class ProductsImageCollectionViewCell: UICollectionViewCell {
    
    static let identifier: String = "ProductsImageCollectionViewCell"

    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func configure(image: String) {
        if let productImage: UIImage = UIImage(named: image) {
            imageView.image = productImage
        } else {
            imageView.image = UIImage(named: "image-not-available")
        }
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "ProductsImageCollectionViewCell", bundle: nil)
    }

}
