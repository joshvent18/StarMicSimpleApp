//
//  ProductSelectionViewCell.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/15/22.
//

import UIKit

class ProductSelectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var productLabel: UILabel!
    
    static let identifier: String = "ProductSelectionViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        // set cell radius
        cellCornerRadius(view: self, radius: 10.0)
    }
    
    public func configureCell(image: String, type: String) {
        imageView.image   = UIImage(named: image)
        productLabel.text = type
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "ProductSelectionViewCell", bundle: nil)
    }

}
