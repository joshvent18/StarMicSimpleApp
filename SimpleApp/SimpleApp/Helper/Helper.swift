//
//  Helper.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 10/4/22.
//

import Foundation

class Helper {
    static func parseJSON() {
        guard let path = Bundle.main.path(forResource: "products", ofType: "json") else {
            return 
        }
        
        let url = URL(fileURLWithPath: path)
        
        do {
            let data = try Data(contentsOf: url)

        } catch {
            print("Error: \(error)")
        }
    }
    
    static func parseData(resource: String, type: String) -> Data {
        if let path = Bundle.main.path(forResource: resource, ofType: type) {
            
            let url = URL(fileURLWithPath: path)
            
            do {
                let data = try Data(contentsOf: url)
                return data
            } catch {
                print("Error: \(error)")
            }
        } else {
            print("Incorrect file path")
        }
        
        return Data()
    }
}
