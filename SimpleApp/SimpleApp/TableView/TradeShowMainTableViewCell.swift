//
//  TradeShowMainTableViewCell.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/25/22.
//

import UIKit

protocol TradeShowMainTableViewDelegate: AnyObject {
    func deviceStatus( cell: TradeShowMainTableViewCell)
    func delete(_ cell: TradeShowMainTableViewCell)
    func information(_ cell: TradeShowMainTableViewCell)
    func settings(_ cell: TradeShowMainTableViewCell)
    func testPrint(_ cell: TradeShowMainTableViewCell)
}

class TradeShowMainTableViewCell: UITableViewCell {
    
    weak var cellDelegate: TradeShowMainTableViewDelegate?
    static let identifier = "TradeShowMainTableViewCell"
    
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var printButton: UIButton!
    @IBOutlet weak var bluetoothIcon: UIButton!
    @IBOutlet weak var infoIconButton: UIButton!
    @IBOutlet weak var deviceStatusButton: UIButton!
    
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var connectionStatusLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var printerLabel: UILabel!
    @IBOutlet weak var macAddressLabel: UILabel!
    
    var cellIndex: Int?
    var connected: Bool?
    
    // UI Elements
    let textColorConnected: UIColor = UIColor.systemGreen
    let textColorDisconnected: UIColor = UIColor.systemRed
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        macAddressLabel.text = ""
        connected = false
    }
    
    func configureCell(name: String, type: String, image: String, index: Int, macAddress: String,
                       isConnected: Bool) {
        
        let modelType           = type.lowercased()
        cellIndex               = index
        printerLabel.text       = name
        
        if let imageName = UIImage(named: image) {
            productImage.image = imageName
        } else {
            productImage.image = UIImage(named: "image-not-available")
        }
        
        // show settings and print button if printer,
        // otherwise hide it
        if(modelType.contains("thermal") || modelType.contains("impact")
           || modelType.contains("portable") || modelType.contains("kiosk")) {
            typeLabel.text = type + " Printer"
            let connectionImage = UIImage(systemName: "dot.radiowaves.up.forward")
            settingsButton.setTitle("Connect", for: .normal)
            settingsButton.setImage(connectionImage, for: .normal)
            showPrintButton()
            showSettingsButton()
            showDeviceStatus()
        } else {
            let connectionImage = UIImage(systemName: "dot.radiowaves.up.forward")
            settingsButton.setTitle("Connect", for: .normal)
            settingsButton.setImage(connectionImage, for: .normal)
            typeLabel.text = type
            hidePrintButton()
            hideSettingsButton()
        }
        
        // device is connected
        if isConnected && macAddress.isEmpty {      // bluetooth or USB
            deviceConnected(macAddress: "", status: "Connected")
            hideBluetooth()
            enablePrintButton()
            enableStatusButton()
        } else if isConnected && !macAddress.isEmpty {  // ethernet
            deviceConnected(macAddress: "MAC: \(macAddress)", status: "Connected")
            hideBluetooth()
            enablePrintButton()
            enableStatusButton()
        } else {
        // device is disconnected
            var image: UIImage?     = nil
            var buttonTitle: String = ""
            
            let device = modelType.filter {!$0.isWhitespace}
            
            switch device {
            case ProductCategory.scanners.rawValue:
                image = UIImage(systemName: "barcode.viewfinder")
                buttonTitle = "Scan"
                showSettingsButton()
                hidePrintButton()
                hideMacAddressLabel()
                hideDeviceStatus()
            case "cashdrawer",
                 ProductCategory.stands.rawValue:
                hideMacAddressLabel()
                hideSettingsButton()
                hidePrintButton()
                hideDeviceStatus()
            case "posscales":
                image = UIImage(systemName: "dot.radiowaves.up.forward")
                buttonTitle = "Connect"
                showSettingsButton()
                hidePrintButton()
                hideMacAddressLabel()
            default:
                image = UIImage(systemName: "dot.radiowaves.up.forward")
                buttonTitle = "Connect"
                showMacAddressLabel()
            }
            
            // This is an exception since mPOP is both cash drawer and printer
            if name.lowercased().contains("mpop") {
                typeLabel.text = "Printer / Cash Drawer"
                showMacAddressLabel()
                showPrintButton()
                showSettingsButton()
                
                if isConnected {
                    enablePrintButton()
                    enableStatusButton()
                    if macAddress.isEmpty {
                        deviceConnected(macAddress: "", status: "Connected")
                        showBluetooth()
                        enablePrintButton()
                    } else {
                        deviceConnected(macAddress: "MAC: \(macAddress)", status: "Connected")
                        hideBluetooth()
                        enablePrintButton()
                    }
                } else {
                    buttonTitle = "Connect"
                    disablePrintButton()
                    disableStatusButton()
                }
            }
            
            if type == "Kiosk" {
                disableSettingButton()
            }
                            
            settingsButton.setImage(image, for: .normal)
            settingsButton.setTitle(buttonTitle, for: .normal)
            settingsButton.backgroundColor = UIColor(named: "star-blue")
            deviceDisconnected()
            hideBluetooth()
            disablePrintButton()
            disableStatusButton()
        }
                
    }
    
    func disablePrintButton() {
        printButton.isUserInteractionEnabled = false
        printButton.alpha                    = 0.2
        printButton.backgroundColor          = UIColor.systemGray
        printButton.tintColor                = UIColor.white
    }
    
    func enablePrintButton() {
        printButton.isUserInteractionEnabled = true
        printButton.alpha                    = 1.0
        printButton.backgroundColor          = UIColor(named: "star-blue")
        printButton.tintColor                = UIColor.white
    }
    
    func disableSettingButton() {
        settingsButton.isUserInteractionEnabled = false
        settingsButton.alpha           = 0.2
        settingsButton.backgroundColor = UIColor.systemGray
        settingsButton.tintColor       = UIColor.white
    }
    
    func disableStatusButton() {
        deviceStatusButton.isUserInteractionEnabled = false
        deviceStatusButton.alpha           = 0.2
        deviceStatusButton.backgroundColor = UIColor.systemGray
        deviceStatusButton.tintColor       = UIColor.white
    }
    
    func enableStatusButton() {
        deviceStatusButton.isUserInteractionEnabled = true
        deviceStatusButton.alpha                    = 1.0
        deviceStatusButton.backgroundColor          = UIColor(named: "star-blue")
        deviceStatusButton.tintColor                = UIColor.white
    }
    
    func hideMacAddressLabel() {
        macAddressLabel.isHidden = true
    }
    
    func showMacAddressLabel() {
        macAddressLabel.isHidden = false
    }
    
    func deviceConnected(macAddress: String, status: String) {
        macAddressLabel.textColor  = UIColor.black
        macAddressLabel.text       = macAddress
        connectionStatusLabel.textColor = textColorConnected
        connectionStatusLabel.text = status
        
        settingsButton.backgroundColor = UIColor.systemRed
        settingsButton.setTitle("Disconnect", for: .normal)
    }
    
    func deviceDisconnected() {
        macAddressLabel.text            = ""
        macAddressLabel.textColor       = textColorDisconnected
        connectionStatusLabel.textColor = textColorDisconnected
        connectionStatusLabel.text      = "Disconnected"
    }
    
    func hidePrintButton() {
        printButton.isHidden = true
    }
    
    func showPrintButton() {
        printButton.isHidden = false
    }
    
    func hideSettingsButton() {
        settingsButton.isHidden = true
    }
    
    func showSettingsButton() {
        settingsButton.isHidden = false
    }
    
    func showBluetooth() {
        bluetoothIcon.isHidden = false
    }
    
    func hideBluetooth() {
        bluetoothIcon.isHidden = true
    }
    
    func showDeviceStatus() {
        deviceStatusButton.isHidden = false
    }
    
    func hideDeviceStatus() {
        deviceStatusButton.isHidden = true
    }
        
    @IBAction func infoTapped(_ sender: Any) {
        cellDelegate?.information(self)
    }
        
    @IBAction func printTapped(_ sender: Any) {
        cellDelegate?.testPrint(self)
    }
    
    @IBAction func settingsTapped(_ sender: Any) {
        cellDelegate?.settings(self)
    }
    
    @IBAction func peripheralsTapped(_ sender: Any) {
        cellDelegate?.deviceStatus(cell: self)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
}

//            if modelType.contains(ProductCategory.scanners.rawValue) {
//                image = UIImage(systemName: "barcode.viewfinder")
//                buttonTitle = "Scan"
//                showSettingsButton()
//                hidePrintButton()
//                hideMacAddressLabel()
//            } else {
//                image = UIImage(systemName: "dot.radiowaves.up.forward")
//                buttonTitle = "Connect"
//                showMacAddressLabel()
//            }
