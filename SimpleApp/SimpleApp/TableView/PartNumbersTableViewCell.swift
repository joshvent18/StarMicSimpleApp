//
//  PartNumbersTableViewCell.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 10/26/22.
//

import UIKit

class PartNumbersTableViewCell: UITableViewCell {
    
    static let identifier = "PartNumbersTableViewCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var partNumberLabel: UILabel!
    @IBOutlet weak var interfaceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /*
        Configure layout for each cells in .xib
        - Parameters:
            - modelName:    Product model name
            - partNo:       Product part number
            - details:      Product details such as interface, information, etc....
     */
    public func configure(modelName: String, partNo: String, details: String) {
        nameLabel.text       = modelName
        partNumberLabel.text = partNo
        interfaceLabel.text  = details
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "PartNumbersTableViewCell", bundle: nil)
    }
    
}
