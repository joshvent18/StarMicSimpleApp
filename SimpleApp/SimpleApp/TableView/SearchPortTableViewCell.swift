//
//  SearchPortTableViewCell.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/27/22.
//

import UIKit

class SearchPortTableViewCell: UITableViewCell {
    
    static let identifier: String = "SearchPortTableViewCell"
    
    @IBOutlet weak var modelNameLabel: UILabel!
    @IBOutlet weak var portNameLabel: UILabel!
    @IBOutlet weak var macAddressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(model: String, portName: String, macAddress: String) {
        modelNameLabel.text  = model
        portNameLabel.text   = portName
        macAddressLabel.text = macAddress
    }
    
    static func nib() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
}
