//
//  DeviceStatusTableViewCell.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 2/28/24.
//

import UIKit

class DeviceStatusTableViewCell: UITableViewCell {
    
    static let identifier: String = "DeviceStatusTableViewCell"
    
    @IBOutlet weak var statusNameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(statusArray: Status) {

        statusNameLabel.text = statusArray.statusType
        statusLabel.text = statusArray.status
                
        if statusArray.state == 1 || statusArray.state == 3 {
            if statusArray.statusType == "Paper Near End" {
                statusLabel.textColor = .systemYellow
            } else if statusArray.statusType == "Paper End" {
                statusLabel.textColor = .systemRed
            } else {
                statusLabel.textColor = .systemBlue
            }
        } else {
            if statusArray.status == "Paper Ready" {
                statusLabel.textColor = .systemBlue
            } else {
                statusLabel.textColor = .systemRed
            }
        }
    }
    
    static func nib() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
}
