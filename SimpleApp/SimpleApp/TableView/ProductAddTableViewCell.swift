//
//  ProductAddTableViewCell.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 11/18/22.
//

import UIKit

protocol ProductAddTableViewDelegate: AnyObject {
    func didTapAddButton(_ cell: ProductAddTableViewCell)
}

class ProductAddTableViewCell: UITableViewCell {
    
    weak var cellDelegate: ProductAddTableViewDelegate?
    static let identifier = "ProductAddTableViewCell"
    
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var addButton: UIButton!
    
    var cellIndex: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let buttonRadius = addButton.frame.height / 2
        
        // Initialization code
        addButton.layer.cornerRadius = buttonRadius
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(item: String, image: String, type: String, index: Int, added: Bool) {
        
        cellIndex      = index
        itemLabel.text = item
        typeLabel.text = type
        
        if let itemImage = UIImage(named: image) {
            productImage.image = itemImage
        } else {
            productImage.image = UIImage(named: "image-not-available")
        }
        
        // Set default state for button in each cell
        // if added -> button is red
        // if not added -> button is blue
        if added {
            addButton.backgroundColor = UIColor.systemRed
            addButton.setTitle("Remove", for: .normal)
        } else {
            addButton.backgroundColor = UIColor.systemBlue
            addButton.setTitle("Add", for: .normal)
        }
        
    }
    
    @IBAction func tapAddButton(_ sender: Any) {
        cellDelegate?.didTapAddButton(self)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "ProductAddTableViewCell", bundle: nil)
    }
    
}
