//
//  PrinterFunctions.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 10/13/22.
//

import UIKit

class PrinterFunctions {
    
    // MARK: - CREATE RECEIPTS
    static func createTextReceiptData(_ emulation: StarIoExtEmulation,
                                      utf8: Bool,
                                      receiptType: Int) -> Data {
        
        let localizeReceipts: ILocalizeReceipts
        let builder: ISCBBuilder = StarIoExt.createCommandBuilder(emulation)
//        let builder2: ISCBBuilder = StarIoExt.createCommandBuilder(StarIoExtEmulation.escPos)
        
        builder.beginDocument()
        
        localizeReceipts = EnglishReceipts()
   
        switch receiptType {
        case 1:
            localizeReceipts.append3inchTextReceiptData(builder, utf8: true)
        case 2:
            localizeReceipts.append3inchHospitalityTextReceiptData(builder, utf8: true)
        case 3:
            localizeReceipts.append39mmTextReceiptData(builder, utf8: true)
        default:
            localizeReceipts.append3inchHospitalityTextReceiptData(builder, utf8: true)
        }
        
        builder.appendCutPaper(SCBCutPaperAction.partialCutWithFeed)
        
        builder.endDocument()
        
        return builder.commands.copy() as! Data
    }
    
    static func create2InchTextReceiptData(_ emulation: StarIoExtEmulation,
                                           utf8: Bool,
                                           receiptType: Int) -> Data {
        
        let localizeReceipts: ILocalizeReceipts
        let builder: ISCBBuilder = StarIoExt.createCommandBuilder(emulation)
        
        builder.beginDocument()
        
        localizeReceipts = EnglishReceipts()
        
        switch receiptType {
        case 1:
            localizeReceipts.append2inchTextReceiptData(builder, utf8: true)
        case 2:
            localizeReceipts.append2InchHospitalityReceiptData(builder, utf8: true)
        default:
            localizeReceipts.append2inchTextReceiptData(builder, utf8: true)
        }
        
        builder.appendCutPaper(SCBCutPaperAction.partialCutWithFeed)
        
        builder.endDocument()
        
        return builder.commands.copy() as! Data
    }
    
    static func create39mmTextReceiptData(_ emulation: StarIoExtEmulation, utf8: Bool, receiptType: Int) -> Data {
        let localizeReceipts: ILocalizeReceipts
        let builder: ISCBBuilder = StarIoExt.createCommandBuilder(emulation)
        
        builder.beginDocument()
        
        localizeReceipts = EnglishReceipts()
        
        localizeReceipts.append39mmTextReceiptData(builder, utf8: true)
        
        builder.appendCutPaper(SCBCutPaperAction.fullCutWithFeed)
        
        builder.endDocument()
        
        return builder.commands.copy() as! Data
    }
        
    static func createPrintableAreaData(_ emulation: StarIoExtEmulation, type: SCBPrintableAreaType) -> Data {
        let localizeReceipts: ILocalizeReceipts
        let builder: ISCBBuilder = StarIoExt.createCommandBuilder(emulation)
        
        builder.beginDocument()
        
        localizeReceipts = EnglishReceipts()
        localizeReceipts.createPrintableAreaData(builder, type: SCBPrintableAreaType.type1)
        
        builder.appendCutPaper(SCBCutPaperAction.partialCutWithFeed)
        
        builder.endDocument()
        
        return builder.commands as Data
    }
    
    static func createRasterReceiptData(_ emulation: StarIoExtEmulation, utf8: Bool, receiptType: Int) -> Data {
        
        let image: UIImage?
        let localizeReceipts: ILocalizeReceipts
        let builder: ISCBBuilder = StarIoExt.createCommandBuilder(emulation)
        
        builder.beginDocument()
        
        localizeReceipts = EnglishReceipts()
        
        switch receiptType {
        case 1:
            image = localizeReceipts.createRasterReceiptData()
            builder.append(SCBPrintableAreaType.standard)
            builder.appendBitmap(image, diffusion: false)
        case 2:
            image = localizeReceipts.createRasterHospitalityReceiptData()
            builder.append(SCBPrintableAreaType.standard)
            builder.appendBitmap(image, diffusion: false)
        case 3:
            localizeReceipts.create39mmRasterReceiptData(builder)
        default:
            image = localizeReceipts.createRasterReceiptData()
            builder.append(SCBPrintableAreaType.standard)
            builder.appendBitmap(image, diffusion: false)
        }
                        
        builder.appendCutPaper(SCBCutPaperAction.partialCutWithFeed)
        
        builder.endDocument()
        
        return builder.commands.copy() as! Data
    }
    
    static func createConnectionSuccessfulTestReceiptData(_ emulation: StarIoExtEmulation,
                                                          utf8: Bool, modelName: String,
                                                          portName: String, macAddress: String) -> Data {
        
        let printerModel: PrinterModel = PrinterModel()
        let localizeReceipts: ILocalizeReceipts
        let builder: ISCBBuilder = StarIoExt.createCommandBuilder(emulation)
        
        builder.beginDocument()
        
        localizeReceipts = EnglishReceipts()
        
        if modelName == printerModel.tsp100iii {
            localizeReceipts.append3inchConnectionRasterReceiptData(builder, utf8: true, model: modelName, port: portName, mac: macAddress)
        } else {
            localizeReceipts.append3inchConnectionTextReceiptData(builder, utf8: true, model: modelName, port: portName,
                                                                  mac: macAddress)
        }

        builder.appendCutPaper(SCBCutPaperAction.partialCutWithFeed)
        
        builder.endDocument()
        
        return builder.commands.copy() as! Data
    }
    
    static func triggerPeripheral(_ emulation: StarIoExtEmulation,
                                  utf8: Bool, channel: SCBPeripheralChannel) -> Data {
        
        let builder: ISCBBuilder = StarIoExt.createCommandBuilder(emulation)
        
        builder.beginDocument()
        
        builder.appendPeripheral(channel)
//        builder.appendSound(channel)
        
        builder.endDocument()
        
        return builder.commands.copy() as! Data
    }
        
}
