//
//  Structs.swift
//  SimpleApp
//
//  Created by Joshua Ventocilla on 7/17/23.
//

// Printer model names according to the SDK
struct PrinterModel {
    let mcp20: String     = "mC-Print2"
    let mcp21: String     = "mC-Print2"
    let mcp30: String     = "mC-Print3"
    let mcp31: String     = "mC-Print3"
    let mclabel: String   = "mC-Label3"
    let mpop: String      = "mPOP®"
    let tsp100iii: String = "TSP143III"
    let tsp100iv: String  = "TSP143IV"
    let tsp100ivsk: String = "TSP143IV SK"
    let tsp650ii: String  = "TSP654II"
    let tsp700ii: String  = "TSP743II"
    let tsp800ii: String  = "TSP847II"
    let sp700: String     = "SP742"
    let sm_s210i: String  = "SM-S210"
    let sm_s230i: String  = "SM-S230"
    let sm_t300: String   = "SM-T300"
    let sm_t400: String   = "SM-T400"
    let sm_l200: String   = "SM-L200"
    let sm_l300: String   = "SM-L300"
}

// Printer interfaces according to the SDK
struct ModelPortName {
    // LAN Interface
    let mcprint21  = "MCP21 (STR-001)"
    let mcprint20  = "MCP20 (STR-001)"
    let mcprint31  = "MCP31 (STR-001)"
    let mcprint30  = "MCP30 (STR-001)"
    let mcLabel    = "MCL32 (STR-001)"
    let tsp143iiiL = "TSP143III LAN"
    let tsp143iiiW = "TSP143IIIW (STR_T-001)"
    let tsp143iv   = "TSP143IV (STR-001)"           // same for SK model
    let tsp650ii   = "TSP654 (STR_T-001)"
    let tsp700ii   = "TSP743II (STR_T-001)"
    let tsp800ii   = "TSP847II (STR_T-001)"
    let sp700      = "SP742 (STR-001)"
    
    // Bluetooth Interface
    let bt_mcprint20L  = "MCP20L"
    let bt_mcprint21LB = "MCP21LB"
    let bt_mcprint31LB = "MCP31LB"
    let bt_mcprint31CBI = "MCP31CBI"
    let bt_mcprint31L  = "MCP31L"
    let bt_mclabel3CI  = "MCL32CI"
    let bt_mclabel3CBI = "MCL32CBI"
    let bt_mpop        = "POP10"
    let bt_mpopci      = "POP10CI"
    let bt_mpopcbi     = "POP10CBI"
    let bt_tsp143iiiUWHT  = "TSP143IIIU WT"
    let bt_tsp143iiiUGY = "TSP143IIIU GY"
    let bt_tsp143iiiBiWT = "TSP143IIIBI WT"
    let bt_tsp143iiiBiGY = "TSP143IIIBI GY"
    let bt_tsp650ii = "Star Micronics"
    let bt_tsp700ii = "Star Micronics"
    let bt_tsp800ii = "Star Micronics"
    let bt_sp700    = "Star Micronics"
    
    // Portable Printers
    let sm_s210i = "SM-S210"
    let sm_s230i = "SM-S230"
    let sm_t300  = "SM-T300"
    let sm_t400  = "SM-T400"
    let sm_l200  = "SM-L200"
    let sm_l300  = "SM-L300"
}

struct Status {
    var status: String
    var statusType: String
    var state: UInt32
}
